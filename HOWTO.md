# HOWTO

This file is separated into 2 sections: installation and usage.

## Installation

### Manual

Follow the steps in `build.ssh`.

Further information:

- [Lemur](http://www.lemurproject.org/lemur/install.php)
- [Indri](http://lemur.sourceforge.net/indri/)

### Automatic

We've created a [Vagrant](vagrantup.com) machine that can be used to start up a Ubuntu 14.04 instance and then build Lemur, Indri, and DCT.

`Vagrantfile`: specifies the VM settings
`build.sh`: script to do the necessary building.

Simply use Vagrant as you normally would, e.g. `vagrant up && vagrant ssh`. Then run `build.sh`.

## Usage

If you're in the VM, the instructions below will be relative to `~/dct/.`

1. Update the parameters in `clustering.conf`.
2. `clustering clustering.conf`

DCT dependencies on having an index built from your data. This is done using IndriBuildIndex under the `bin` folder where you have Indri installed. An example configuration file is in `index/IndriBuildIndexTestData.conf`.

---

DCT Clustering -- Dynamic Clustering of Streaming Short Documents
Copyright (c) 2016, Shangsong Liang. All rights reserved.
