
# Makefile
# date:     2016-02-10
# version:  1.0
# Shangsong Liang
# email: S.Liang@ucl.ac.uk, Department of CS, University College London, UK


# Lemur installation
Lemur_dir = /home/$(USER)/libs/lemur
Indri_dir = /home/$(USER)/libs/indri/include/indri
# Lemur_dir = /zfs/ilps-plexer/sliang/softwarePath
# Indri_dir = /zfs/ilps-plexer/sliang/softwarePath/include/indri

#%.o: %.cpp
#	$(CXX) $(CXXFLAGS) -o $@ -c $<
#%: %.o
#	$(CXX) $(CXXFLAGS) -o $@ $< $(CPPLDFLAGS)

# binary
BIN = clustering

# source directory
SRCDIR = src

# C++ source files
CPPSRC = $(wildcard $(SRCDIR)/*.cpp)

# object directory
OBJDIR = obj

# objects
OBJS = $(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(wildcard $(SRCDIR)/*.cpp))


#INCPATH = -I${Lemur_dir}/include -Iinclude
#INCPATH = -I${Indri_dir}:${Lemur_dir}/include -Iinclude
INCPATH = -I${Lemur_dir}/include -Iinclude  -I${Indri_dir} -Iinclude
LIBPATH = ${Lemur_dir}/lib
LIB = lemur
CXX = g++
#CXX = /usr/bin/g++
CXXFLAGS  = -DPACKAGE_NAME=\"CLUSTERING\" -DPACKAGE_TARNAME=\"clustering\" -DPACKAGE_VERSION=\"0.9\" -DPACKAGE_STRING=\"CLUSTERING\ 0.9\" -DPACKAGE_BUGREPORT=\"S.Liang@ucl.ac.uk\" -DYYTEXT_POINTER=1 -DHAVE_LIBM=1 -DHAVE_LIBPTHREAD=1 -DHAVE_LIBZ=1 -DHAVE_LIBIBERTY=1 -DHAVE_NAMESPACES= -DISNAN_IN_NAMESPACE_STD= -DISNAN_IN_NAMESPACE_GNU_CXX= -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE=1 -DHAVE_FSEEKO=1 -DHAVE_MKSTEMP=1 -DHAVE_MKSTEMPS=1 -DP_HAS_ATOMIC_INT=1 -DP_NEEDS_GNU_CXX_NAMESPACE=1 -DWITH_DISTRIB=0 -DWITH_SUMMARIZATION=0 -DNDEBUG=1 -DWITH_CLUSTER=0  -g -O2 $(INCPATH)

CPPLDFLAGS  =  -L$(LIBPATH) -l$(LIB) -lz -lpthread -lm


# object files
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	# Use this line if
	#$(CXX) --std=c++11 $(CXXFLAGS) -o $@ -c $<
	# Use this line if
	$(CXX) $(CXXFLAGS) -o $@ -c $<

# target file
$(BIN): $(OBJS)
	#$(CXX) --std=c++11 $(CXXFLAGS) -o $@ $^ $(CPPLDFLAGS) # -fopenmp
	$(CXX) $(CXXFLAGS) -o $@ $^ $(CPPLDFLAGS) -fopenmp


all: $(BIN)

clean:
	rm $(BIN) $(OBJDIR)/*.o
