echo "Installing needed packages"
sudo apt-get install -y make g++-4.4 build-essential linux-headers-$(uname -r) zlib1g-dev unzip wget
echo "Linking up GCC"
sudo rm /usr/bin/{gcc,g++}
sudo ln -s /usr/bin/gcc-4.4 /usr/bin/gcc
sudo ln -s /usr/bin/g++-4.4 /usr/bin/g++

# Lemur
echo "Installing Lemur"
cd ~/
mkdir -p ~/libs/lemur
wget -q http://netix.dl.sourceforge.net/project/lemur/lemur/lemur-toolkit-4.12/lemur-4.12.tar.gz
gunzip lemur-4.12.tar.gz
tar xf lemur-4.12.tar
cd lemur-4.12
chmod +x configure
make clean
./configure --prefix=/home/$USER/libs/lemur
make
make install
cd ~/
rm -rf lemur-4.12*
exit 0

# Indri
echo "Installing Indri"
cd ~/
mkdir -p ~/libs/indri
wget -q  http://netix.dl.sourceforge.net/project/lemur/lemur/indri-5.9/indri-5.9.tar.gz
gunzip indri-5.9.tar.gz
tar xf indri-5.9.tar
cd indri-5.9
chmod +x configure
make clean
./configure --prefix=/home/$USER/libs/indri
make
make install
cd ~/
rm -rf indri-5.9*

# DCT
echo "Installing DCT"
cd ~/
# GET THE LATEST URL!
# TODO Change to use git clone instead so we don't need to update this specific URL each time
wget -q https://bitbucket.org/sliang1/dct/get/f2f19691359f.zip
unzip f2f19691359f.zip
cd sliang1-dct-f2f19691359f
make clean
make