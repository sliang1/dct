//
//  Association.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/4/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#ifndef DivStr_Association_h
#define DivStr_Association_h

#include "DataTypesTwo.h"
#include "Param.hpp"
#include "dirent.h"
#include "QueryRep.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>

// Lemur
#include "Param.hpp"
#include "Index.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

namespace lss{
    class Association
    {
    public:
        Association(const lemur::api::Index& index, const lss::QueryRep& query): index_(index), queries_(query){};
        void initSmoothing(); // Init smoothing settings
        virtual lss::QUEDOCSCO_T getQueDocSco();
        void priDocCon(std::string docStr); //print document content
    protected:
        const lemur::api::Index& index_;
        const lss::QueryRep& queries_;
        std::string smoothingMethod_; // Smoothing method
        double smoothingParam_; // Smoothing parameter
        int topDocNum_; // Number of top documents considered per query
    };
}

#endif














