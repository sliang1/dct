//
//  DataTypesTwo.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 9/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef _DATATYPESTWO_H
#define _DATATYPESTWO_H

#include <map>
#include <string>
#include <utility>
#include <list>
#include <vector>
#include <queue>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>

namespace lss
{
    typedef std::map<std::string, double> DOCSCO_T;
    typedef std::pair<std::string, double> DOCSCO_PAIR_T;
    typedef std::map<std::string, DOCSCO_T> QUEDOCSCO_T;//query-document-score(also for rank)
    typedef std::pair<std::string, DOCSCO_T> QUEDOCSCO_PAIR_T;
    
    typedef std::map<std::string, DOCSCO_T> DOCDOCSCO_T;//document-document-score(KL distance score)
    typedef std::pair<std::string, DOCSCO_T> DOCDOCSCO_PAIR_T;
    typedef std::map<std::string, int> QUEIND_T;//query-index
    typedef std::pair<std::string, int> QUEIND_PAIR_T;
    
    typedef std::map<std::string, char> DOCCHA_T;
    typedef std::pair<std::string, char> DOCCHA_PAIR_T;
    
    typedef struct{std::string docStr; double sco;} DOCSTRSCO_T;
    
    typedef struct{int day; int hour;} TIME_T;
    typedef std::map<std::string, TIME_T> DOCTIME_T;
    typedef std::pair<std::string, TIME_T> DOCTIME_PAIR_T;
    typedef std::map<std::string, int> DOCTIMEIND_T;
    typedef std::pair<std::string, int> DOCTIMEIND_PAIR_T;
    
    typedef struct{double from; double to; double score;} TIMEINTERVAL_T;//document burst from x to y, score is the score for the interval
    typedef std::map<std::string, std::vector<TIMEINTERVAL_T> > QUEINTERVAL_T;
    typedef std::pair<std::string, std::vector<TIMEINTERVAL_T> > QUEINTERVAL_PAIR_T;
    
    typedef std::pair<const double*, const double*> PType;
    
    typedef std::map<int, std::vector<std::string> > TIMINDDOCVEC_T; //time index - document id string
    typedef std::pair<int, std::vector<std::string> > TIMINDDOCVEC_PAIR_T;
    typedef std::map<std::string, TIMINDDOCVEC_T> QUETIMINDDOCVEC_T;
    typedef std::pair<std::string, TIMINDDOCVEC_T> QUETIMINDDOCVEC_PAIR_T;
    
    typedef std::map<std::string, DOCDOCSCO_T> QUEDOCDOCSCO_T;
    typedef std::pair<std::string, DOCDOCSCO_T> QUEDOCDOCSCO_PAIR_T; //query-doc-doc-score for burst doc-doc score for a given query
    
    typedef std::map<std::string, std::vector<std::string> > QUEDOCINBUR_T; //query-doc(the documents are in the bursts)
    typedef std::pair<std::string, std::vector<std::string> > QUEDOCINBUR_PAIR_T;
    
    typedef std::map<int, std::string> DOCINDDOC_T; //(document index)-document in array
    typedef std::pair<int, std::string> DOCINDDOC_PAIR_T;
    
    typedef std::map<std::string, int> DOCDOCIND_T; // document-(document index) in array
    typedef std::pair<std::string, int> DOCDOCIND_PAIR_T;
    
    typedef struct{int ind; double cov;} INDCOV_T;// document index- coviariance
    
    //query-burst-score
    typedef std::map<std::string, double> BURSCO_T;//burst-score
    typedef std::pair<std::string, double> BURSCO_PAIR_T;
    typedef std::map<std::string, BURSCO_T> QUEBURSCO_T;//query-burst-score
    typedef std::pair<std::string, BURSCO_T> QUEBURSCO_PAIR_T;
    
    //query-burst-document-score
    typedef std::map<std::string, DOCSCO_T> BURDOCSCO_T;//burst-document-score
    typedef std::pair<std::string, DOCSCO_T> BURDOCSCO_PAIR_T;
    typedef std::map<std::string, BURDOCSCO_T> QUEBURDOCSCO_T;//query-burst-document(in the burst or all the documents)-score
    typedef std::pair<std::string, BURDOCSCO_T> QUEBURDOCSCO_PAIR_T;
    
    typedef std::vector<std::vector<double> > TWODIM_T; //two-dimension vector,served as arrays
    typedef std::map<std::string, double> TERFRE_T; //term-frequency
    typedef std::pair<std::string, double> TERFRE_PAIR_T;
    
    typedef std::pair<std::string, std::string> STRSTR_T;
    typedef std::pair<std::string, std::string> STRSTR_PAIR_T;
    
    /// Internal ID encoding
    typedef int ID_T;
    
    /// External ID encoding
    typedef std::string EXID_T;
    
    /// Probability distribution
    typedef std::map< ID_T, double > DISTR_T;
    
    /// Association results
    typedef std::vector< std::vector< double > > ASSOCRES_T;
    
    typedef struct{std::string tim; std::string sta; std::string end;} TIMSTAEND_T;
    
    //lss: map external documentID (the first column of q-0xx) to internal documentID (indexed by M)
    typedef std::map<std::string, int> EXTID2INTID_T;
    typedef std::map<int, std::string> INTID2EXTID_T;
    
    typedef std::map<std::string, double> TOKSCO_T;
    typedef std::pair<std::string, double> TOKSCO_PAIR_T;
    typedef std::map<std::string, TOKSCO_T> DOCTOKSCO_T;
    typedef std::pair<std::string, TOKSCO_T> DOCTOKSCO_PAIR_T;
    
    typedef std::map<std::string, std::vector<double> > STRSCOVEC_T;
    typedef std::pair<std::string, std::vector<double> > STRSCOVEC_PAIR_T;
    
    typedef std::map<std::string, std::queue<std::string> > STRSTRQUE_T;
    typedef std::pair<std::string, std::queue<std::string> > STRSTRQUE_PAIR_T;
    
    typedef std::map<std::string, std::vector<std::string> > STRSTRVEC_T;
    typedef std::pair<std::string, std::vector<std::string> > STRSTRVEC_PAIR_T;
    
    
    
    #define TEMPFILE1 "fileNames.temp"
    #define TEMPFILE2 "fileNames2.temp"
    #define TRECPARMETER " -q -m all_trec "
    #define SMALLVALUE 0.001
    
    
}
#endif