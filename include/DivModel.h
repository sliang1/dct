//
//  DivModel.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/20/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//
// iniEstTyp is the type of initialization way before doing estimation

#ifndef DivStr_DivModel_h
#define DivStr_DivModel_h

#include "constants.h"
#include "model.h"
#include "DataTypesTwo.h"

#include <vector>

// Lemur
#include "Param.hpp"
#include "Index.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

// for digammal
#include <algorithm>

using namespace std;
using namespace lss;

#ifndef M_PIl
/** The constant Pi in high precision */
#define M_PIl 3.1415926535897932384626433832795029L
#endif
#ifndef M_GAMMAl
/** Euler's constant in high precision */
#define M_GAMMAl 0.5772156649015328606065120900824024L
#endif
#ifndef M_LN2l
/** the natural logarithm of 2 in high precision */
#define M_LN2l 0.6931471805599453094172321214581766L
#endif

namespace lss{
    class DivModel : public model
    {
    public:
        string dfileDir;
        string othFilDir;
        string iniEstTyp; // the score of iniEstTyp can be divStream_whole (Dirichelet Multinomial Mixture Model using the whole dataset, kdd 2014), divStream_time (our method, using the data up to the query time) 
        int* zd_who; // zd_who[i] is the topic assignment for document, size is M (total number of documents), diriechlet multinomial mixture model for the whole dataset
        int* mz_who; // mz_who[i] is the total number of documents that are signed to the same topic, size is K (total number of topics), dirichlet multinomial mixture model for the whole dataset
        int* nz_who; //nz_who[i] is the total number of words assigned to topic i, size is K (tota number of topics), dirichlet multinomial mxiture model for the whole dataset
        int* newz_who; // newz_who[i] is the current topic assigned to document i
        std::vector<lss::STRSTR_T> iDStr2Dat; // mininum id for each day, idstr-datastr
        std::string curDatStr; //up to the day
        std::vector<std::string> datVec;
        
        
        // for time-aware diversification in data stream
        double* alpha_tim; //alpha_tim[i] is the alpha score for topic i, size is K
        double* beta_tim; //beta_tim[i] is the beta score for word i, size is V
        int* mz_tim; // mz_tim[i] is the total number of documents that are signed to the same topic, size is K (total number of topics), time-aware diversification short text model
        double* theta_tim; //theta_tim[i] is the probability of i-th topic, size is K (total number of topics), time-aware diversification short text model
        double** phi_tim; //topic-word distributions, size K X V, time-aware diversification short text model
        int* zd_tim; // zd_tim[i] is the topic assignment for document, size is M (total number of documents), diriechlet multinomial mixture model for the whole dataset with time information
        int* nz_tim; // nz_tim[i] is the total number of words assigned to topic i, size is K
        std::vector<int> worDocIDVec; //working document set for time-aware diversification in data stream
        std::string curWorQue; //current working query
        
        // parse command line to get options
        int parse_args(std::string str);
        
        // initialize the model
        int init(std::string str);
        
        // init for estimation
        int init_est();
        int init_est_divStream_whole(); // initialize the estimation for diversification in stream with the whole dataset
        int init_est_divStream_time(); // initialize the estimation for time-aware diversificaton in stream
        //int init_est_divStream_time_begin(); // initialize the estimation for time-aware diversification in stream for the days in the first interval
        void estimate();
        void estimate_divStream_whole();
        void estimate_divStream_time();
        void estimate_divStream_time_begin(); // estimate for the days in the first interval
        void estimate_divStream_time2();
        int sampling_divStream_whole(int m);
        int sampling_divStream_time(int m);
        void compute_theta_divStream_whole();
        void compute_theta_divStream_time();
        void compute_phi_divStream_whole();
        void compute_phi_divStream_time();
        int save_model_divStream_whole(string model_name);
        int save_model_divStream_time(string model_name);
        int save_model_tassign_divStream_whole(string filename);
        int save_model_tassign_divStream_time(string filename);
        int save_model_theta_divStream_whole(string filename);
        int save_model_theta_divStream_time(string filename);
        int save_model_twords_divStream_time(string filename);
        int save_model_phi_divStream_time(string filename);
        int save_model_others_divStream_time(string filename);
        int save_model_alpha_divStream_time(string filename);
        int save_model_beta_divStream_time(string filename);
        
        void update_Alpha_divStream_time();
        void update_Beta_divStream_time();
        long double digammal(long double x); // get the Psi() score
        ~DivModel();
    private:
        std::vector<int> getWorDocIndSet(int intervalInd, std::string maxDocStr);//get working documents' Index set
    };
}


#endif
