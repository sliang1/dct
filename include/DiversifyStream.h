//
//  DiversifyStream.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/3/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#ifndef DivStr_DiversifyStream_h
#define DivStr_DiversifyStream_h

#include "DataTypesTwo.h"
#include "Param.hpp"
#include "dirent.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>

// Lemur
#include "Param.hpp"
#include "Index.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

namespace lss{
    class DiversifyStream
    {
    public:
        DiversifyStream();
        void init();
    private:
        const lemur::api::Index* index_;
        void openIndex();
        void closeIndex();
    };
}

#endif





