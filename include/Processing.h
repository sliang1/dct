//
//  Processing.h
//  divStrPro
//
//  Created by Shangsong Liang on 7/14/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#ifndef _PROCESSING_H
#define _PROCESSING_H

#include "DataTypesTwo.h"
#include "Param.hpp"
#include "dirent.h"

// Lemur
#include "Param.hpp"
#include "Index.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

namespace lss
{
    class Processing
    {
    public:
        Processing(const lemur::api::Index& index): index_(index){};
        Processing();
        void getSubSetDat();
        void getSubSetDatRemRet();
        void mvFilIntGro(); //move file into groups for indexing
        void extHasTag(); //extract hashtags for generating query
        void extHasMuiTag(); //extract hastags co-occur in the same post for generating query
        void proRemInvHasTag(); //remove invalid hastag
        void finQue(); //find useful queries for doing experiments
        void finQue2();
        void forQue(); //formatting queries
        void wriDayStaEndDocID(); //for writing the document ID of start and end ID in each day
        void wriDayStaEndDocID2();
        void itePriFil(std::string str); // iterate print file name for a folder
        //std::vector<std::string> retSubDir(std::string str); // return sub-folder for a folder
        void forInpDat(); //inputDir is the input director file string, outputDir is the output director file string
        std::string priDocCon(std::string docStr); //print document content
        std::string getStdOutFroCom(std::string cmd); //get the output from a command
        std::vector<std::string> filVec;
        std::string getPat(const std::string& str); //get the path of the file
        std::string getFilNam(const std::string& str); //get the name of the file
        
        //for processing MRR
        lss::DOCSCO_T getDocSco(std::string forQueFil, std::string thetaFil, const int topK); // return top-k documents with ranked score
        lss::DOCSCO_T getDocSco_lan(std::string forQueFil, std::string thetaFil, const int topK); // return top-k documents with ranked score
        
        void combResLisFroEachQue(std::string resDir, std::string resDir2); // combine the retrieval lists from each query into a single query
        
        
        void forResFilForQue(std::string retrResDir1, std::string retrResDir3); // format result file for query such that ndeval can be used
        void forResFilForQue2(std::string filStr1, std::string filStr2);
        
        

    protected:
        const lemur::api::Index& index_;
    };
}


#endif
