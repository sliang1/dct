//
//  QueryRep.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/3/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#ifndef DivStr_QueryRep_h
#define DivStr_QueryRep_h

// lss
#include "DataTypesTwo.h"

// Lemur
#include "Index.hpp"
#include "SimpleKLRetMethod.hpp"

namespace lss{
    class QueryRep
    {
    public:
        QueryRep() {};
        ~QueryRep() {};
        void loadQueries( const lemur::api::Index& index );
        const int queryNum() const; //number of queries
        const std::string queryID( const int& qID ) const; //reture query ID as string
        const DISTR_T query( const int& qID ) const;
        lemur::retrieval::SimpleKLQueryModel* query(
                                                    const lemur::api::Index& index,
                                                    const int& qID ) const;
        void display(const lemur::api::Index& index);
    private:
        std::vector< std::string > queryID_;
        /// Queries
        /// (i.e., ptq_[q] holds p(t|q) for query q)
        std::vector< DISTR_T > ptq_;
    };
}


#endif
