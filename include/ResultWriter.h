//
//  ResultWriter.h
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 9/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#ifndef _RESULTWRITER_H
#define _RESULTWRITER_H

#include "DataTypesTwo.h"

#include <string>
#include <vector>
#include <dirent.h>

namespace lss
{
    class ResultWriter
    {
    public:
        void writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, const std::string subResPat="");
        //void writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, lss::QUEDOCSCO_T& qTopDS, const double percent); // write result and also get (percent) top documents.
        void writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, lss::QUEDOCSCO_T& qTopDS, const double topDocNum, bool isForDiv=false); // write result for diversification.
        bool dirExists(const char* dirPath); // see if the directory exists
        void wriQueLDAFil(lss::QUEDOCSCO_T& qTopDS); // write query-lda files, each file is formated with lda format and name by the query.
        //void writeRank(lss::QUEDOCSCO_T qDS, const std::string dir);
        void writeRank(std::string que, lss::DOCSCO_T dS, const std::string filNam, const std::string ranNam); // write result file for evaluation with append to the file
    private:
        std::string savPath_;
        std::string qrelFil_;
        std::string qrelGFil_; //gdeval.pl
        std::string qrelNFil_; //ndeval
        std::string trecEvalFil_;
        std::string gdevalEvalFil_;
        std::string ndevalEvalFil_;
    };
}

#endif







