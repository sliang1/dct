//
//  Association.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/4/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#include <stdio.h>
#include "Association.h"

// Lemur
#include "Param.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>

void
lss::Association::initSmoothing()
{
    smoothingMethod_ = lemur::api::ParamGetString( "smoothingMethod" );
    smoothingParam_ = lemur::api::ParamGetDouble( "smoothingParam", 0.5 );
    if(smoothingMethod_=="") //default smoothing parameter setting
    {
        smoothingMethod_="jm";
        smoothingParam_=0.5;
    }
    else if(smoothingMethod_=="JM" || smoothingMethod_=="jm" || smoothingMethod_=="Jelnek-Mercer" || smoothingMethod_=="jelnek-mercer")
    {
        smoothingMethod_="jm";
    }
    topDocNum_ = lemur::api::ParamGetInt("topDocNum", 100);
}

lss::QUEDOCSCO_T
lss::Association::getQueDocSco()
{
    lss::QUEDOCSCO_T qDS;
    return qDS;
}

void
lss::Association::priDocCon(std::string docStr)
{
    int docID=index_.document(docStr);
    int termID;
    double prob;
    
    lemur::langmod::DocUnigramCounter collCounter(docID, index_);
    lemur::langmod::MLUnigramLM MLCollModel(collCounter, index_.termLexiconID());
    //lemur::langmod::UnigramLM *collModel=&MLCollModel;
    
    //collModel->startIteration();
    MLCollModel.startIteration();
    for(; MLCollModel.hasMore();)
    {
        MLCollModel.nextWordProb(termID, prob);
        std::cout<<index_.term(termID)<<" ";
        //std::cout<<"term="<<index_.term(termID)<<" number of this term="<<index_.termCount(termID)<<" ";
    }
    std::cout<<std::endl;
    //delete collModel;
}

/*
 double
 lss::Distance::computeKLScor(std::string doc1, std::string doc2)
 {
 int docID1=index_->document(doc1);
 int docID2=index_->document(doc2);
 double docLen1=index_->docLength(docID1);
 double docLen2=index_->docLength(docID2);
 double skLScor=0.0;//symmetric K-L divergence score
 
 lemur::langmod::DocUnigramCounter* collCounter1
 = new lemur::langmod::DocUnigramCounter(docID1,*index_ );
 lemur::langmod::DocUnigramCounter* collCounter2
 = new lemur::langmod::DocUnigramCounter(docID2, *index_);
 lemur::langmod::DocUnigramCounter* collCounter
 =new lemur::langmod::DocUnigramCounter(*index_);
 
 lemur::langmod::UnigramLM *collModel1
 = new lemur::langmod::MLUnigramLM( *collCounter1, index_->termLexiconID() );
 lemur::langmod::UnigramLM *collModel2
 = new lemur::langmod::MLUnigramLM( *collCounter2, index_->termLexiconID() );
 lemur::langmod::UnigramLM *collModel
 = new lemur::langmod::MLUnigramLM( *collCounter, index_->termLexiconID() );
 
 int termID=0;
 double prob=0.0;
 double probSum=0.0;
 double smoPro1=0.0;//smoothing p(w|Q)
 double smoPro2=0.0;//smoothing p(w|D)
 double muPwC=0.0;//mu*p(w|c)
 double sumWInQ=0.0;//term in Q
 double sumWInDNotInQ=0.0;//term in D but not in Q
 double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
 std::string termStr;
 collModel1->startIteration();
 for(;collModel1->hasMore();)
 {
 collModel1->nextWordProb(termID, prob);
 muPwC=this->smoPar_*collModel->prob(termID);
 if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
 {
 smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
 smoPro2=(collModel2->prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
 sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
 }
 }
 skLScor=sumWInQ;
 collModel2->startIteration();
 for(;collModel2->hasMore();)
 {
 collModel2->nextWordProb(termID, prob);
 muPwC=this->smoPar_*collModel->prob(termID);
 if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
 {
 smoPro1=(collModel1->prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
 smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
 sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
 }
 }
 skLScor=skLScor+sumWInDNotInQ;
 
 delete collCounter1;
 delete collCounter2;
 delete collModel1;
 delete collModel2;
 return 0.5*skLScor;
 }
 */











