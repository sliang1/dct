//
//  DivModel.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/20/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>

#include "DivModel.h"
#include "utils.h"

int
lss::DivModel::parse_args(std::string str)
{
    return utils::parse_args(this, str);
}

int
lss::DivModel::init(std::string str)
{
    this->set_default_values();
    if(this->parse_args(str))
    {
        return 1;
    }
    curWorQue=str;
    
    if (model_status == MODEL_STATUS_EST) {
        if(iniEstTyp=="") // for lda
        {
            if(init_est())
            {
                return 1;
            }
        }
        else if(iniEstTyp=="divStream_whole") //initialize estimaiton for data stream
        {
            if(init_est_divStream_whole())
            {
                return 1;
            }
        }
        else if(iniEstTyp=="divStream_time")
        {
            if(init_est_divStream_time())
            {
                return 1;
            }
        }
        /* // estimating the model from scratch
        if (init_est()) {
            return 1;
        }*/
        
    } else if (model_status == MODEL_STATUS_ESTC) {
        // estimating the model from a previously estimated one
        if (init_estc()) {
            return 1;
        }
        
    } else if (model_status == MODEL_STATUS_INF) {
        // do inference
        if (init_inf()) {
            return 1;
        }
    }
    
    return 0;
}

int
lss::DivModel::init_est()
{
    int m, n, w, k;
    
    p = new double[K];
    
    // + read training data
    ptrndata = new dataset;
    std::cout<<"dir="<<dir<<std::endl;
    std::cout<<"dfile="<<dfile<<std::endl;
    std::cout<<"wordmapfile="<<wordmapfile<<std::endl;
    std::cout<<"dfileDir="<<dfileDir<<std::endl;
    std::cout<<"othFilDir="<<othFilDir<<std::endl;
    
    if(ptrndata->read_trndata(dfileDir+"/"+dfile, othFilDir+"/"+wordmapfile))
    {
        printf("Fail to read training data!\n");
        return 1;
    }

    
    /*if (ptrndata->read_trndata(dir + dfile, dir + wordmapfile)) {
        printf("Fail to read training data!\n");
        return 1;
    }*/
    
    
    // + allocate memory and assign values for variables
    M = ptrndata->M;
    V = ptrndata->V;
    // K: from command line or default value
    // alpha, beta: from command line or default values
    // niters, savestep: from command line or default values
    
    nw = new int*[V];
    for (w = 0; w < V; w++) {
        nw[w] = new int[K];
        for (k = 0; k < K; k++) {
            nw[w][k] = 0;
        }
    }
    
    nd = new int*[M];
    for (m = 0; m < M; m++) {
        nd[m] = new int[K];
        for (k = 0; k < K; k++) {
            nd[m][k] = 0;
        }
    }
    
    nwsum = new int[K];
    for (k = 0; k < K; k++) {
        nwsum[k] = 0;
    }
    
    ndsum = new int[M];
    for (m = 0; m < M; m++) {
        ndsum[m] = 0;
    }
    
    //srandom(time(0)); // initialize for random number generation
    z = new int*[M];
    for (m = 0; m < ptrndata->M; m++) {
        int N = ptrndata->docs[m]->length;
        z[m] = new int[N];
        
        // initialize for z
        for (n = 0; n < N; n++) {
            int topic = (int)(((double)random() / RAND_MAX) * K);
            z[m][n] = topic;
            
            // number of instances of word i assigned to topic j
            nw[ptrndata->docs[m]->words[n]][topic] += 1;
            // number of words in document i assigned to topic j
            nd[m][topic] += 1;
            // total number of words assigned to topic j
            nwsum[topic] += 1;
        }
        // total number of words in document i
        ndsum[m] = N;
    }
    
    theta = new double*[M];
    for (m = 0; m < M; m++) {
        theta[m] = new double[K];
    }
    
    phi = new double*[K];
    for (k = 0; k < K; k++) {
        phi[k] = new double[V];
    }
    
    return 0;
}

void
lss::DivModel::estimate()
{
    if (twords > 0) {
        // print out top words per topic
        //dataset::read_wordmap(dir + wordmapfile, &id2word);
        dataset::read_wordmap(othFilDir+"/"+wordmapfile, &id2word);
    }
    
    printf("Sampling %d iterations!\n", niters);
    
    int last_iter = liter;
    for (liter = last_iter + 1; liter <= niters + last_iter; liter++) {
        //printf("Iteration %d ...\n", liter);
        
        // for all z_i
        for (int m = 0; m < M; m++) {
            for (int n = 0; n < ptrndata->docs[m]->length; n++) {
                // (z_i = z[m][n])
                // sample from p(z_i|z_-i, w)
                int topic = sampling(m, n);
                z[m][n] = topic;
            }
        }
        
        if (savestep > 0) {
            if (liter % savestep == 0) {
                // saving the model
                printf("Saving the model at iteration %d ...\n", liter);
                compute_theta();
                compute_phi();
                save_model(utils::generate_model_name(liter));
            }
        }
    }
    
    printf("Gibbs sampling completed!\n");
    printf("Saving the final model!\n");
    compute_theta();
    compute_phi();
    liter--;
    if(save_model(utils::generate_model_name(-1)))
    {
        std::cout<<"Can not save the model."<<std::endl;
    }
    /*EXTID2INTID_T::iterator it;
    for(it=this->ptrndata->extIDStr2IntID.begin(); it!=this->ptrndata->extIDStr2IntID.end(); it++)
    {
        std::cout<<"doc="<<it->first<<" id="<<it->second<<std::endl;
    }*/
}

int
lss::DivModel::init_est_divStream_whole()
{
    int m, n, w, k;
    
    p = new double[K];
    
    // + read training data
    ptrndata = new dataset;
    std::cout<<"dir="<<dir<<std::endl;
    std::cout<<"dfile="<<dfile<<std::endl;
    std::cout<<"wordmapfile="<<wordmapfile<<std::endl;
    std::cout<<"dfileDir="<<dfileDir<<std::endl;
    std::cout<<"othFilDir="<<othFilDir<<std::endl;
    
    if(ptrndata->read_trndata(dfileDir+"/"+dfile, othFilDir+"/"+wordmapfile))
    {
        printf("Fail to read training data!\n");
        return 1;
    }
    
    /*if (ptrndata->read_trndata(dir + dfile, dir + wordmapfile)) {
     printf("Fail to read training data!\n");
     return 1;
     }*/
    
    
    // + allocate memory and assign values for variables
    M = ptrndata->M;
    V = ptrndata->V;
    // K: from command line or default value
    // alpha, beta: from command line or default values
    // niters, savestep: from command line or default values
    
    nw = new int*[V];
    for (w = 0; w < V; w++) {
        nw[w] = new int[K];
        for (k = 0; k < K; k++) {
            nw[w][k] = 0;
        }
    }
    
    nd = new int*[M];
    for (m = 0; m < M; m++) {
        nd[m] = new int[K];
        for (k = 0; k < K; k++) {
            nd[m][k] = 0;
        }
    }
    
    nwsum = new int[K];
    for (k = 0; k < K; k++) {
        nwsum[k] = 0;
    }
    
    ndsum = new int[M];
    for (m = 0; m < M; m++) {
        ndsum[m] = 0;
    }
    
    zd_who = new int[M];
    mz_who = new int[K];
    nz_who = new int[K];
    newz_who = new int[M];
    for(k=0; k<K; k++)
    {
        mz_who[k]=0;
        nz_who[k]=0;
    }
    //srandom(time(0));
    for(m=0; m<ptrndata->M; m++)
    {
        int topic;
        topic=(int)(((double)random()/RAND_MAX) * K);
        zd_who[m]=topic;
        newz_who[m]=topic;
        mz_who[topic]=mz_who[topic]+1;
        nz_who[topic]=nz_who[topic] + ptrndata->docs[m]->length;
        nwsum[topic]=nwsum[topic] + ptrndata->docs[m]->length;
        for(n=0; n<ptrndata->docs[m]->length; n++)
        {
            nw[ptrndata->docs[m]->words[n]][topic] = nw[ptrndata->docs[m]->words[n]][topic] +1;
        }
    }
    
    
    
    /*
    srandom(time(0)); // initialize for random number generation
    z = new int*[M];
    for (m = 0; m < ptrndata->M; m++) {
        int N = ptrndata->docs[m]->length;
        z[m] = new int[N];
        
        // initialize for z
        for (n = 0; n < N; n++) {
            int topic = (int)(((double)random() / RAND_MAX) * K);
            z[m][n] = topic;
            
            // number of instances of word i assigned to topic j
            nw[ptrndata->docs[m]->words[n]][topic] += 1;
            // number of words in document i assigned to topic j
            nd[m][topic] += 1;
            // total number of words assigned to topic j
            nwsum[topic] += 1;
        }
        // total number of words in document i
        ndsum[m] = N;
    }*/
    
    theta = new double*[M];
    for (m = 0; m < M; m++) {
        theta[m] = new double[K];
    }
    
    phi = new double*[K];
    for (k = 0; k < K; k++) {
        phi[k] = new double[V];
    }
    
    return 0;
}

void
lss::DivModel::estimate_divStream_time_begin()
{
    int m, n;
    int topic;
    
    //srandom(time(0));
    for(m=0; m<worDocIDVec.size(); m++)
    {
        topic=(int)(((double)random()/RAND_MAX) * K);
        zd_tim[worDocIDVec[m]]=topic;
        mz_tim[topic]=mz_tim[topic]+1;
        nz_tim[topic]=nz_tim[topic] + ptrndata->docs[worDocIDVec[m]]->length;
        for(n=0; n<ptrndata->docs[worDocIDVec[m]]->length; n++)
        {
            nw[ptrndata->docs[worDocIDVec[m]]->words[n]][topic] = nw[ptrndata->docs[worDocIDVec[m]]->words[n]][topic] + 1;
        }
    }
    for(topic=0; topic<K; topic++)
    {
        std::cout<<"nz_tim["<<topic<<"]="<<nz_tim[topic]<<std::endl;
    }
    
    /*int m, n, w, k;
    
    p = new double[K];
    
    // + read training data
    ptrndata = new dataset;
    std::cout<<"dir="<<dir<<std::endl;
    std::cout<<"dfile="<<dfile<<std::endl;
    std::cout<<"wordmapfile="<<wordmapfile<<std::endl;
    std::cout<<"dfileDir="<<dfileDir<<std::endl;
    std::cout<<"othFilDir="<<othFilDir<<std::endl;
    
    if(ptrndata->read_trndata(dfileDir+"/"+dfile, othFilDir+"/"+wordmapfile))
    {
        printf("Fail to read training data!\n");
    }
    
    
    // + allocate memory and assign values for variables
    M = ptrndata->M;
    V = ptrndata->V;
    // K: from command line or default value
    // alpha, beta: from command line or default values
    // niters, savestep: from command line or default values
    
    nw = new int*[V];
    for (w = 0; w < V; w++) {
        nw[w] = new int[K];
        for (k = 0; k < K; k++) {
            nw[w][k] = 0;
        }
    }
    
    nd = new int*[M];
    for (m = 0; m < M; m++) {
        nd[m] = new int[K];
        for (k = 0; k < K; k++) {
            nd[m][k] = 0;
        }
    }
    
    nwsum = new int[K];
    for (k = 0; k < K; k++) {
        nwsum[k] = 0;
    }
    
    ndsum = new int[M];
    for (m = 0; m < M; m++) {
        ndsum[m] = 0;
    }
    
    zd_who = new int[M];
    mz_who = new int[K];
    nz_who = new int[K];
    newz_who = new int[M];
    for(k=0; k<K; k++)
    {
        mz_who[k]=0;
        nz_who[k]=0;
    }
    srandom(time(0));
    for(m=0; m<ptrndata->M; m++)
    {
        int topic;
        topic=(int)(((double)random()/RAND_MAX) * K);
        zd_who[m]=topic;
        newz_who[m]=topic;
        mz_who[topic]=mz_who[topic]+1;
        nz_who[topic]=nz_who[topic] + ptrndata->docs[m]->length;
        for(n=0; n<ptrndata->docs[m]->length; n++)
        {
            nw[ptrndata->docs[m]->words[n]][topic] = nw[ptrndata->docs[m]->words[n]][topic] +1;
        }
    }
    
    theta = new double*[M];
    for (m = 0; m < M; m++) {
        theta[m] = new double[K];
    }
    
    phi = new double*[K];
    for (k = 0; k < K; k++) {
        phi[k] = new double[V];
    }
    */
}

int
lss::DivModel::init_est_divStream_time()
{
    int m, n, w, k;
    
    p = new double[K];
    std::string minTimFilStr;
    
    // + read training data
    ptrndata = new dataset;
    std::cout<<"dir="<<dir<<std::endl;
    std::cout<<"dfile="<<dfile<<std::endl;
    std::cout<<"wordmapfile="<<wordmapfile<<std::endl;
    std::cout<<"dfileDir="<<dfileDir<<std::endl;
    std::cout<<"othFilDir="<<othFilDir<<std::endl;
    
    minTimFilStr=lemur::api::ParamGetString("minTimFil", "");
    if(utils::read_minDat(minTimFilStr, this))
    {
        std::cout<<"Can not read the minTimFil. The papameter minTimFil may not be set."<<std::endl;
        return 1;
    }
    
    if(ptrndata->read_trndata(dfileDir+"/"+dfile, othFilDir+"/"+wordmapfile))
    {
        printf("Fail to read training data!\n");
        return 1;
    }
    
    /*if (ptrndata->read_trndata(dir + dfile, dir + wordmapfile)) {
     printf("Fail to read training data!\n");
     return 1;
     }*/
    
    
    // + allocate memory and assign values for variables
    M = ptrndata->M;
    V = ptrndata->V;
    
    alpha_tim = new double[K];
    beta_tim = new double[V];
    
   

    theta_tim = new double[K];
    /*phi_tim = new double*[V];
    for(w=0; w<V; w++)
    {
        phi_tim[w]=new double[K];
    }*/
    phi_tim = new double*[K];
    for(k=0; k<K; k++)
    {
        phi_tim[k]=new double[V];
    }
    zd_tim = new int[M];
    mz_tim = new int[K];
    nz_tim = new int[K];
    for(k=0; k<K; k++)
    {
        alpha_tim[k]=alpha;
    }
    for(w=0; w<V; w++)
    {
        beta_tim[w]=beta;
    }
    for(k=0; k<K; k++) // initilize for time-aware short diversification model
    {
        theta_tim[k]=1.0/(K*1.0);
        mz_tim[k]=0;
        nz_tim[k]=0;
        for(w=0; w<V; w++)
        {
            phi_tim[k][w]=1.0/(V*1.0);
        }
    }
    
    
    //%%%%%%%%%%%%%
    // K: from command line or default value
    // alpha, beta: from command line or default values
    // niters, savestep: from command line or default values
    
    nw = new int*[V];
    for (w = 0; w < V; w++) {
        nw[w] = new int[K];
        for (k = 0; k < K; k++) {
            nw[w][k] = 0;
        }
    }
    
    /*nd = new int*[M];
    for (m = 0; m < M; m++) {
        nd[m] = new int[K];
        for (k = 0; k < K; k++) {
            nd[m][k] = 0;
        }
    }*/
    
    nwsum = new int[K];
    for (k = 0; k < K; k++) {
        nwsum[k] = 0;
    }
    
    /*ndsum = new int[M];
    for (m = 0; m < M; m++) {
        ndsum[m] = 0;
    }*/
    
    /*zd_who = new int[M];
    mz_who = new int[K];
    nz_who = new int[K];
    newz_who = new int[M];
    for(k=0; k<K; k++)
    {
        mz_who[k]=0;
        nz_who[k]=0;
    }
    srandom(time(0));
    for(m=0; m<ptrndata->M; m++)
    {
        int topic;
        topic=(int)(((double)random()/RAND_MAX) * K);
        zd_who[m]=topic;
        newz_who[m]=topic;
        mz_who[topic]=mz_who[topic]+1;
        nz_who[topic]=nz_who[topic] + ptrndata->docs[m]->length;
        for(n=0; n<ptrndata->docs[m]->length; n++)
        {
            nw[ptrndata->docs[m]->words[n]][topic] = nw[ptrndata->docs[m]->words[n]][topic] +1;
        }
    }*/
    
    theta = new double*[M];
    for (m = 0; m < M; m++) {
        theta[m] = new double[K];
    }
    
    phi = new double*[K];
    for (k = 0; k < K; k++) {
        phi[k] = new double[V];
    }
    
    return 0;
}





void
lss::DivModel::estimate_divStream_whole()
{
    int m=0;
    int topic;
    if (twords > 0) {
        // print out top words per topic
        //dataset::read_wordmap(dir + wordmapfile, &id2word);
        dataset::read_wordmap(othFilDir+"/"+wordmapfile, &id2word);
    }
    
    printf("Sampling %d iterations!\n", niters);
    int last_iter = liter;
    for(liter =  last_iter +1; liter <= niters + last_iter; liter++)
    {
        for(m=0; m< M; m++)
        {
            topic=sampling_divStream_whole(m);
            zd_who[m]=topic;
        }
        if (savestep > 0)
        {
            if (liter % savestep == 0)
            {
                // saving the model
                printf("Saving the model at iteration %d ...\n", liter);
                compute_theta();
                compute_phi();
                save_model_divStream_whole(utils::generate_model_name(liter));
            }
        }//if (savestep > 0)
    }
    
    printf("Gibbs sampling completed!\n");
    printf("Saving the final model!\n");
    compute_theta_divStream_whole();
    compute_phi_divStream_whole();
    liter--;
    if(save_model_divStream_whole(utils::generate_model_name(-1)))
    {
        std::cout<<"Can not save the model."<<std::endl;
    }
    /*EXTID2INTID_T::iterator it;
     for(it=this->ptrndata->extIDStr2IntID.begin(); it!=this->ptrndata->extIDStr2IntID.end(); it++)
     {
     std::cout<<"doc="<<it->first<<" id="<<it->second<<std::endl;
     }*/
}

void
lss::DivModel::estimate_divStream_time()
{
    bool hasDone; //for the first dayIntervalSep
    int dayIntervalSep;
    int minDocForIter;
    int m=0;
    int topic;
    lemur::api::ParamGet("dayIntervalSep", dayIntervalSep);
    lemur::api::ParamGet("minDocForIter", minDocForIter);
    hasDone=false;
    //std::vector<int> worDocIDVec; //working document set.
    EXTID2INTID_T::iterator it;
    std::string queTimIndDirStr;
    std::string queTimIndFil;
    std::fstream input;
    
    queTimIndDirStr=lemur::api::ParamGetString("worQueTimIndDir", "");
    queTimIndFil=queTimIndDirStr+"/"+curWorQue;
    input.open(queTimIndFil.c_str(), std::ios::out);
    if (twords > 0) {
        // print out top words per topic
        //dataset::read_wordmap(dir + wordmapfile, &id2word);
        dataset::read_wordmap(othFilDir+"/"+wordmapfile, &id2word);
    }
    /*for(int i=0; i<iDStr2Dat.size(); i++)
    {
        std::cout<<iDStr2Dat[i].first<<"  "<<iDStr2Dat[i].second<<std::endl;
    }*/
    for(int i=0; i<iDStr2Dat.size(); i++) //i is the i-th day
    {
        if((hasDone==false) && ((i+1)%dayIntervalSep==0))//the first dayIntervalSep days
        {
            worDocIDVec.clear();
            worDocIDVec=getWorDocIndSet(i, iDStr2Dat[i].first);
            //if(worDocVec.size()>=(0.5 * ptrndata->extIDStr2IntID.size()/iDStr2Dat.size()*dayIntervalSep))
            if(worDocIDVec.size()>=minDocForIter)
            {
                hasDone=true;
                curDatStr=iDStr2Dat[i].second;
                estimate_divStream_time_begin(); // initialization
                estimate_divStream_time2();
                input<<iDStr2Dat[i].second<<std::endl; // record the up to day
            }
        }
        else if((hasDone==true && (i+1)%dayIntervalSep==0) || i==(iDStr2Dat.size()-1))
        {
            worDocIDVec.clear();
            worDocIDVec=getWorDocIndSet(i, iDStr2Dat[i].first);
            curDatStr=iDStr2Dat[i].second;
            estimate_divStream_time_begin();
            estimate_divStream_time2();
            input<<iDStr2Dat[i].second<<std::endl;
        }
    } //for(int i=0; i<iDStr2Dat.size(); i++)
    
    /*std::cout<<"iDStr2Dat size="<<iDStr2Dat.size()<<std::endl;
    
    for(unsigned long j=0; j<iDStr2Dat.size(); j++)
    {
        std::cout<<iDStr2Dat[j].first<<"  ,"<<iDStr2Dat[j].second<<std::endl;
    }*/
    
    /*std::vector<lss::STRSTR_T>::iterator it;
    for(it=iDStr2Dat.begin(); it!=iDStr2Dat.end(); it++)
    {
        std::cout<<""<<it->first<<" "<<it->second<<std::endl;
    }*/
    input.close();
}

void
lss::DivModel::estimate_divStream_time2()
{
    int m, topic;
    
    printf("Sampling %d iterations!\n", niters);
    int last_iter = liter;
    for(liter = last_iter +1; liter <= niters + last_iter; liter++)
    {
        for(m=0; m<worDocIDVec.size(); m++)
        {
            //topic=sampling_divStream_whole(worDocIDVec[m]); //*****
            topic=sampling_divStream_time(worDocIDVec[m]);
            zd_tim[worDocIDVec[m]]=topic;
        }
        if(savestep > 0)
        {
            if(liter % savestep ==0)
            {
                // saving the model
                printf("Saving the model at iteration %d ...\n", liter);
                compute_theta();
                compute_phi();
                save_model_divStream_time(utils::generate_model_name(liter));
            }
        } //
    }
    printf("Gibbs sampling completed!\n");
    printf("Saving the final model!\n");
    compute_theta_divStream_time();
    compute_phi_divStream_time();
    liter--;
    if(save_model_divStream_time(utils::generate_model_name(-1)))
    {
        std::cout<<"Can not save the model."<<std::endl;
    }
    update_Alpha_divStream_time();
    update_Beta_divStream_time();
    /*
    printf("Sampling %d iterations!\n", niters);
    int last_iter = liter;
    for(liter =  last_iter +1; liter <= niters + last_iter; liter++)
    {
        for(m=0; m< M; m++)
        {
            topic=sampling_divStream_whole(m);
            zd_who[m]=topic;
        }
        if (savestep > 0)
        {
            if (liter % savestep == 0)
            {
                // saving the model
                printf("Saving the model at iteration %d ...\n", liter);
                compute_theta();
                compute_phi();
                save_model_divStream_whole(utils::generate_model_name(liter));
            }
        }//if (savestep > 0)
    }
    
    printf("Gibbs sampling completed!\n");
    printf("Saving the final model!\n");
    compute_theta_divStream_whole();
    compute_phi_divStream_whole();
    liter--;
    if(save_model_divStream_whole(utils::generate_model_name(-1)))
    {
        std::cout<<"Can not save the model."<<std::endl;
    }*/

}

int
lss::DivModel::sampling_divStream_time(int m)
{
    int topic;
    int w;
    int n;
    int k;
    const int dLen=ptrndata->docs[m]->length; //length of the document
    const int curDocSetSiz=(int)worDocIDVec.size();
    double temp1, temp2;
    double* betaPhiSum; // betaPhiSum[k] is the sum of beta[w]*phi_tim[k][w] the k-th topic
    double alphaThetaSum;
    double u;
    
    topic=zd_tim[m];
    mz_tim[topic]--;
    nz_tim[topic]=nz_tim[topic] - dLen;
    for(n=0; n<dLen; n++)
    {
        nw[ptrndata->docs[m]->words[n]][topic]--;
    }
    betaPhiSum=new double[K];
    for(k=0; k<K; k++)
    {
        betaPhiSum[k]=0.0;
    }
    alphaThetaSum=0.0;
    for(k=0; k<K; k++)
    {
        alphaThetaSum=alphaThetaSum + alpha_tim[k] * theta_tim[k];
        for(w=0; w<V; w++)
        {
            betaPhiSum[k]=betaPhiSum[k] + beta_tim[w] * phi_tim[k][w];
        }
    }
    
    for(k=0; k<K; k++)
    {
        temp1=1.0;
        temp2=1.0;
        for(n=0; n<dLen; n++)
        {
            w=ptrndata->docs[m]->words[n];
            temp1=temp1* (nw[w][k] + beta_tim[w]*phi_tim[k][w]);
            temp2=temp2* (nz_tim[k] + betaPhiSum[k] + n+1 -1);
            //std::cout<<"nw["<<w<<"]["<<k<<"]"<<nw[w][k]<<" beta_tim["<<w<<"]"<<beta_tim[w]<<" phi_tim["<<w<<"]["<<k<<"]="<<phi_tim[k][w]<<std::endl;
        }
        p[k]=(mz_tim[k]+alpha_tim[k]*theta_tim[k])/(curDocSetSiz-1+alphaThetaSum) * temp1/temp2;
        //std::cout<<"temp1="<<temp1<<" temp2="<<temp2<<std::endl;
    }
    for(k=1; k<K; k++)
    {
        p[k] +=p[k-1];
    }
    // scaled sample because of unnormalized p[]
    u = ((double)random() / RAND_MAX) * p[K - 1];
    for(topic=0; topic<K; topic++)
    {
        if (p[topic] > u)
        {
            break;
        }
    }
    mz_tim[topic]++;
    nz_tim[topic]=nz_tim[topic]+dLen;
    for(n=0; n<dLen; n++)
    {
        nw[ptrndata->docs[m]->words[n]][topic]++;
    }
    for(k=0; k<K; k++)
    {
        if(nz_tim[k]<0 || nz_tim[k]>curDocSetSiz*0.9)
        {
            //std::cout<<"k="<<k<<std::endl;
        }
    }
    //std::cout<<"M="<<curDocSetSiz<<" "<<nz_tim[2]<<" "<<1.0/(curDocSetSiz*nz_tim[2])<<std::endl;
    if(betaPhiSum)
    {
        delete betaPhiSum;
    }
    return topic;
}

int
lss::DivModel::sampling_divStream_whole(int m)
{
    int topic;
    int w;
    int n;
    int k;
    const int dLen=ptrndata->docs[m]->length; //length of the document
    double temp1, temp2;
    double u;
    
    topic=zd_who[m];
    mz_who[topic]--;
    nwsum[topic]=nwsum[topic]-dLen;
    nz_who[topic]=nz_who[topic] - dLen;
    for(n=0; n<dLen; n++)
    {
        nw[ptrndata->docs[m]->words[n]][topic]--;
    }
    for(k=0; k<K; k++)
    {
        temp1=1.0;
        temp2=1.0;
        for(n=0; n<dLen; n++)
        {
            temp1=temp1* (nw[ptrndata->docs[m]->words[n]][k] + beta);
            temp2=temp2* (nz_who[k] + V * beta + n+1 -1);
        }
        p[k]=(mz_who[k]+alpha)/(M-1+K*alpha) * temp1/temp2;
        //std::cout<<"temp1="<<temp1<<" temp2="<<temp2<<std::endl;
    }
    // cumulate multinomial parameters
    for(k=1; k<K; k++)
    {
        p[k] +=p[k-1];
    }
    // scaled sample because of unnormalized p[]
    u = ((double)random() / RAND_MAX) * p[K - 1];
    for(topic=0; topic<K; topic++)
    {
        if (p[topic] > u)
        {
            break;
        }
    }
    mz_who[topic]++;
    nwsum[topic]=nwsum[topic]+dLen;
    nz_who[topic]=nz_who[topic]+dLen;
    for(n=0; n<dLen; n++)
    {
        nw[ptrndata->docs[m]->words[n]][topic]++;
    }
    
    return topic;
}

void
lss::DivModel::compute_theta_divStream_whole()
{
    int dLen;
    int topic;
    double temp1=1.0;
    double temp2=1.0;
    double sum;

    for(int m=0; m<M; m++)
    {
        dLen=ptrndata->docs[m]->length; //length of the document
        topic=zd_who[m];
        sum=0.0;
        for(int k=0; k<K; k++)
        {
            temp1=1.0;
            temp2=1.0;
            if(topic==k)
            {
                for(int n=0; n<ptrndata->docs[m]->length; n++)
                {
                    temp1=temp1*(nw[ptrndata->docs[m]->words[n]][topic]-1 + beta);
                    temp2=temp2*(nz_who[topic]-1 + V*beta +n+1-1);
                }
                theta[m][topic]=(mz_who[topic]-1+alpha)/(M-1+K*alpha) * temp1/temp2;
                sum+=theta[m][topic];
            }
            else
            {
                for(int n=0; n<ptrndata->docs[m]->length; n++)
                {
                    temp1=temp1*(nw[ptrndata->docs[m]->words[n]][k] + beta);
                    temp2=temp2*(nz_who[k] + V*beta +n+1-1);
                }
                theta[m][k]=(mz_who[k]+alpha)/(M-1+K*alpha) * temp1/temp2;
                //std::cout<<"*"<<theta[m][k]<<std::endl;
                sum+=theta[m][k];
            }
            
        }//for(int k=0; k<K; k++)
        for(int k=0; k<K; k++)//for normalization
        {
            theta[m][k]=theta[m][k]/sum;
        }
    }
}

void
lss::DivModel::compute_theta_divStream_time()
{
    int topic;
    int w;
    int n;
    int k;
    int m;
    int dLen;
    double temp1, temp2;
    double* betaPhiSum;
    double alphaThetaSum;
    double u, sum;
    
    betaPhiSum=new double[K];
    alphaThetaSum=0.0;
    for(k=0; k<K; k++)
    {
        alphaThetaSum=alphaThetaSum + alpha_tim[k] * theta_tim[k];
        for(w=0; w<V; w++)
        {
            betaPhiSum[k]=betaPhiSum[k] + beta_tim[w] * phi_tim[k][w];
        }
    }
    for(unsigned long i=0; i<worDocIDVec.size(); i++)
    {
        m=worDocIDVec[i];
        topic=zd_tim[m];
        dLen=ptrndata->docs[m]->length;
        sum=0.0;
        for(k=0; k<K; k++)
        {
            temp1=1.0;
            temp2=1.0;
            if(k==topic)
            {
                for(n=0; n<dLen; n++)
                {
                    w=ptrndata->docs[m]->words[n];
                    temp1=temp1* (nw[w][k]-1 + beta_tim[w]*phi_tim[k][w]);
                    temp2=temp2* (nz_tim[k]-1 + betaPhiSum[k] + n+1 -1);
                }
                theta[m][k]=(mz_tim[k]-1+alpha_tim[k]*theta_tim[k])/(worDocIDVec.size()-1+alphaThetaSum) * temp1/temp2;
                sum=sum+theta[m][k];
            }
            else
            {
                for(n=0; n<dLen; n++)
                {
                    w=ptrndata->docs[m]->words[n];
                    temp1=temp1* (nw[w][k] + beta_tim[w]*phi_tim[k][w]);
                    temp2=temp2* (nz_tim[k] + betaPhiSum[k] + n+1 -1);
                }
                theta[m][k]=(mz_tim[k]+alpha_tim[k]*theta_tim[k])/(worDocIDVec.size()-1+alphaThetaSum) * temp1/temp2;
                sum=sum+theta[m][k];
            }
        }
        for(int k=0; k<K; k++)//for normalization
        {
            theta[m][k]=theta[m][k]/sum;
        }
    }
}

void
lss::DivModel::compute_phi_divStream_whole()
{
    for (int k = 0; k < K; k++)
    {
        for (int w = 0; w < V; w++)
        {
            phi[k][w] = (nw[w][k] + beta) / (nwsum[k] + V * beta);
        }
    }
}

void
lss::DivModel::compute_phi_divStream_time()
{
    double sum;
    for(int k = 0; k < K; k++)
    {
        sum=0.0;
        for(int w=0; w<V; w++)
        {
            sum=sum+beta_tim[w]*phi_tim[k][w];
        }
        for(int w = 0; w < V; w++)
        {
            //phi[k][w] = (nw[w][k] + beta_tim[w]*phi_tim[k][w]) / (nz_tim[k] + sum);
            phi_tim[k][w] = (nw[w][k] + beta_tim[w]*phi_tim[k][w]) / (nz_tim[k] + sum);
        }
    }
}

int DivModel::save_model_divStream_whole(string model_name) {
    if (save_model_tassign_divStream_whole(dir + model_name + tassign_suffix)) {
        return 1;
    }
    
    if (save_model_others(dir + model_name + others_suffix)) {
        return 1;
    }
    
    if (save_model_theta_divStream_whole(dir + model_name + theta_suffix)) {
        return 1;
    }
    
    if (save_model_phi(dir + model_name + phi_suffix)) {
        return 1;
    }
    
    if (twords > 0) {
        if (save_model_twords(dir + model_name + twords_suffix)) {
            return 1;
        }
    }
    
    return 0;
}

int DivModel::save_model_divStream_time(string model_name)
{
    std::string subDir;
    std::string cmdStr;
    std::fstream isDir;
    if(dir[dir.length()-1]=='/')
    {
        subDir=dir+curDatStr+"/";
    }
    else
    {
        subDir=dir+"/"+curDatStr+"/";
    }
    cmdStr="mkdir "+subDir;
    isDir.open(subDir.c_str(), std::ios::in);
    if(!isDir)
    {
        std::system(cmdStr.c_str());
    }
    else
    {
        isDir.close();
    }
    
    if (save_model_tassign_divStream_time(dir + model_name + tassign_suffix)) {
        return 1;
    }
    
    if (save_model_others_divStream_time(dir + model_name + others_suffix)) {
        return 1;
    }
    
    if (save_model_alpha_divStream_time(dir + model_name + alpha_suffix)) {
        return 1;
    }
    
    if (save_model_beta_divStream_time(dir + model_name + beta_suffix)) {
        return 1;
    }
    
    if (save_model_theta_divStream_time(dir + model_name + theta_suffix)) {
        return 1;
    }
    
    if (save_model_phi_divStream_time(dir + model_name + phi_suffix)) {
        return 1;
    }
    
    if (twords > 0) {
        if (save_model_twords_divStream_time(dir + model_name + twords_suffix)) {
            return 1;
        }
    }
    cmdStr="cp "+dir+"*.* "+subDir;
    std::system(cmdStr.c_str());
    //std::cout<<"cmdStr="<<cmdStr<<std::endl;
    
    return 0;
}

int DivModel::save_model_tassign_divStream_whole(string filename) {
    int i, j;
    //INTID2EXTID_T intID2ExtIDStr;
    INTID2EXTID_T::iterator it; //lss
    
    FILE * fout = fopen(filename.c_str(), "w");
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    
    // wirte docs with topic assignments for words
    for (i = 0; i < ptrndata->M; i++) {
        it=ptrndata->intID2ExtIDStr.find(i); //lss
        fprintf(fout, "%s ", it->second.c_str());
        for (j = 0; j < ptrndata->docs[i]->length; j++) {
            fprintf(fout, "%d:%d ", ptrndata->docs[i]->words[j],zd_who[i]);
        }
        fprintf(fout, "\n");
    }
    
    fclose(fout);
    
    return 0;
}

int DivModel::save_model_tassign_divStream_time(string filename)
{
    int i, j;
    //INTID2EXTID_T intID2ExtIDStr;
    INTID2EXTID_T::iterator it; //lss
    
    FILE * fout = fopen(filename.c_str(), "w");
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    
    // wirte docs with topic assignments for words
    //for (i = 0; i < ptrndata->M; i++) {
    for(i =0; i<worDocIDVec.size(); i++) {
        it=ptrndata->intID2ExtIDStr.find(worDocIDVec[i]); //lss
        fprintf(fout, "%s ", it->second.c_str());
        for (j = 0; j < ptrndata->docs[worDocIDVec[i]]->length; j++) {
            fprintf(fout, "%d:%d ", ptrndata->docs[worDocIDVec[i]]->words[j],zd_tim[worDocIDVec[i]]);
        }
        fprintf(fout, "\n");
    }
    
    fclose(fout);
    
    return 0;
}

int DivModel::save_model_theta_divStream_whole(string filename)
{
    INTID2EXTID_T::iterator it; //lss
    FILE * fout = fopen(filename.c_str(), "w");
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    
    for (int i = 0; i < M; i++) {
        it=ptrndata->intID2ExtIDStr.find(i);
        fprintf(fout, "%s ", it->second.c_str());
        for (int j = 0; j < K; j++) {
            fprintf(fout, "%f ", theta[i][j]);
        }
        fprintf(fout, "\n");
    }
    
    fclose(fout);
    
    return 0;
}

int DivModel::save_model_theta_divStream_time(string filename)
{
    INTID2EXTID_T::iterator it; //lss
    FILE * fout = fopen(filename.c_str(), "w");
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    
    for (int i = 0; i < worDocIDVec.size(); i++) {
        it=ptrndata->intID2ExtIDStr.find(worDocIDVec[i]);
        fprintf(fout, "%s ", it->second.c_str());
        for (int j = 0; j < K; j++) {
            fprintf(fout, "%f ", theta[worDocIDVec[i]][j]);
        }
        fprintf(fout, "\n");
    }
    
    fclose(fout);
    
    return 0;
}

int DivModel::save_model_twords_divStream_time(string filename)
{
    FILE * fout = fopen(filename.c_str(), "w");
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    
    if (twords > V) {
        twords = V;
    }
    mapid2word::iterator it;
    
    for (int k = 0; k < K; k++) {
        vector<pair<int, double> > words_probs;
        pair<int, double> word_prob;
        for (int w = 0; w < V; w++) {
            word_prob.first = w;
            //word_prob.second = phi[k][w];
            word_prob.second = phi_tim[k][w];
            words_probs.push_back(word_prob);
        }
        
        // quick sort to sort word-topic probability
        utils::quicksort(words_probs, 0, words_probs.size() - 1);
        
        fprintf(fout, "Topic %dth:\n", k);
        for (int i = 0; i < twords; i++) {
            it = id2word.find(words_probs[i].first);
            if (it != id2word.end()) {
                fprintf(fout, "\t%s   %f\n", (it->second).c_str(), words_probs[i].second);
            }
        }
    }
    
    fclose(fout);    
    
    return 0;
}

int DivModel::save_model_phi_divStream_time(string filename)
{
    FILE * fout = fopen(filename.c_str(), "w");
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    
    for (int i = 0; i < K; i++) {
        for (int j = 0; j < V; j++) {
            fprintf(fout, "%f ", phi_tim[i][j]);
        }
        fprintf(fout, "\n");
    }
    
    fclose(fout);    
    
    return 0;
}

int DivModel::save_model_others_divStream_time(string filename)
{
    FILE * fout = fopen(filename.c_str(), "w");
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    
    //fprintf(fout, "alpha=%f\n", alpha);
    //fprintf(fout, "beta=%f\n", beta);
    fprintf(fout, "ntopics=%d\n", K);
    //fprintf(fout, "ndocs=%d\n", M);
    fprintf(fout, "ndocs=%d\n", (int)worDocIDVec.size());
    fprintf(fout, "nwords=%d\n", V);
    fprintf(fout, "liter=%d\n", liter);
    
    fclose(fout);
    
    return 0;
}

int DivModel::save_model_alpha_divStream_time(string filename)
{
    FILE * fout = fopen(filename.c_str(), "w");
    double sum=0.0;
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    for(int k=0; k<K; k++)
    {
        sum=sum+alpha_tim[k];
    }
    for(int k=0; k<K; k++)
    {
        fprintf(fout, "%.10f ", alpha_tim[k]/sum);
    }
    fclose(fout);
    return 0;
}

int DivModel::save_model_beta_divStream_time(string filename)
{
    FILE * fout = fopen(filename.c_str(), "w");
    double sum=0.0;
    
    if (!fout) {
        printf("Cannot open file %s to save!\n", filename.c_str());
        return 1;
    }
    for(int w=0; w<V; w++)
    {
        sum=sum+beta_tim[w];
    }
    for(int w=0; w<V; w++)
    {
        fprintf(fout, "%.10f ", beta_tim[w]/sum);
    }
    fclose(fout);
    return 0;
}

std::vector<int>
DivModel::getWorDocIndSet(int intervalInd, std::string maxDocStr)
{
    std::vector<int> resVec;
    EXTID2INTID_T::iterator it;
    int index=0;
    
    if(intervalInd==iDStr2Dat.size()-1)
    {
        for(it=ptrndata->extIDStr2IntID.begin(); it!=ptrndata->extIDStr2IntID.end(); it++)
        {
            resVec.push_back(it->second);
        }
    }
    else
    {
        for(it=ptrndata->extIDStr2IntID.begin(); it!=ptrndata->extIDStr2IntID.end(); it++)
        {
            if(it->first<iDStr2Dat[intervalInd+1].first) //iDStr2Dat[i].first is the documentID that has been increasingly sorted
            {
                resVec.push_back(it->second);
            }
            else
            {
                break;
            }
            //std::cout<<""<<it->first<<" "<<it->second<<std::endl;
        }
    }
    return resVec;
}

void
lss::DivModel::update_Alpha_divStream_time()
{
    int k;
    int fixedPoitIter; //number of fixed-point iterations
    double sum1, sum2;
    double numerator, denominator;
    double changes;
    double priAlpha;
    double fabsSco;
    
    lemur::api::ParamGet("fixedpointIter", fixedPoitIter);
    
    for(int i=0; i<fixedPoitIter; i++)
    {
        sum1=0.0;
        sum2=0.0;
        for(k=0; k<K; k++)
        {
            sum1=sum1 + mz_tim[k] + alpha_tim[k]*theta_tim[k];
            sum2=sum2 + alpha_tim[k]*theta_tim[k];
        }
        denominator=digammal(sum1) - digammal(sum2);
        changes=0.0;
        for(k=0; k<K; k++)
        {
            numerator=alpha_tim[k] * (digammal(mz_tim[k] + alpha_tim[k]*theta_tim[k]) - digammal(alpha_tim[k]*theta_tim[k]));
            priAlpha=alpha_tim[k];
            alpha_tim[k]=numerator/denominator; // update alpha
            fabsSco=fabs(priAlpha-alpha_tim[k]);
            if(fabsSco<0.00001)
            {
                changes=changes+fabsSco;
            }
            else
            {
                changes=changes+1.0;
            }
        }
        if((i>80) && (changes<0.00001*K)) // at least iterate 50 times
        {
            break;
        }
    } //for(int i=0; i<fixedPoitIter; i++)
}

void
lss::DivModel::update_Beta_divStream_time()
{
    int k, w, v;
    int fixedPoitIter; //number of fixed-point iterations
    double numberator, denominator;
    double sum1, sum2;
    double* temp1;
    double* temp2;
    double changes;
    double fabsSco;
    double priBeta;
    
    /*temp1=new double[K];
    temp2=new double[K];
    
    for(k=0; k<K; k++)
    {
        temp1[k]=0.0;
        temp2[k]=0.0;
        for(w=0; w<V; w++)
        {
            temp1[k]=temp1[k] + nw[w][k] + beta_tim[w]*phi_tim[k][w];
            temp2[k]=temp2[k] + beta_tim[w]*phi_tim[k][w];
        }
    }*/
    
    for(int i=0; i<fixedPoitIter; i++)
    {
        changes=0.0;
        for(w=0; w<V; w++)
        {
            numberator=0.0;
            denominator=0.0;
            for(k=0; k<K; k++)
            {
                numberator=numberator + beta_tim[w]*phi_tim[k][w]*(digammal(nw[w][k] + beta_tim[w]*phi_tim[k][w]) - digammal(beta_tim[w]*phi_tim[k][w]));
                sum1=0.0;
                sum2=0.0;
                for(v=0; v<V; v++)
                {
                    sum1=sum1 + nw[v][k] + beta_tim[v]*phi_tim[k][v];
                    sum2=sum2 + beta_tim[v]*phi_tim[k][v];
                }
                denominator=denominator + phi_tim[k][w]*(digammal(sum1) - digammal(sum2));//note **
            }
            priBeta=beta_tim[w];
            beta_tim[w]=numberator/denominator;
            fabsSco=fabs(priBeta-beta_tim[w]);
            if(fabsSco<0.00000001)
            {
                changes=changes+fabsSco;
            }
            else
            {
                changes=changes+1.0;
            }
        } //for(w=0; w<V; w++)
        if(changes<0.00000001*V && i>80) // at least iterate 50 times
        {
            break;
        }
    } //for(int i=0; i<fixedPoitIter; i++)
    /*if(temp1)
    {
        delete temp1;
    }
    if(temp2)
    {
        delete temp2;
    }*/
}

long double
lss::DivModel::digammal(long double x)
{
    /* force into the interval 1..3 */
    if( x < 0.0L )
        return digammal(1.0L-x)+M_PIl/tanl(M_PIl*(1.0L-x)) ;	/* reflection formula */
    else if( x < 1.0L )
        return digammal(1.0L+x)-1.0L/x ;
    else if ( x == 1.0L)
        return -M_GAMMAl ;
    else if ( x == 2.0L)
        return 1.0L-M_GAMMAl ;
    else if ( x == 3.0L)
        return 1.5L-M_GAMMAl ;
    else if ( x > 3.0L)
    /* duplication formula */
        return 0.5L*(digammal(x/2.0L)+digammal((x+1.0L)/2.0L))+M_LN2l ;
    else
    {
        /* Just for your information, the following lines contain
         * the Maple source code to re-generate the table that is
         * eventually becoming the Kncoe[] array below
         * interface(prettyprint=0) :
         * Digits := 63 :
         * r := 0 :
         *
         * for l from 1 to 60 do
         * 	d := binomial(-1/2,l) :
         * 	r := r+d*(-1)^l*(Zeta(2*l+1) -1) ;
         * 	evalf(r) ;
         * 	print(%,evalf(1+Psi(1)-r)) ;
         *o d :
         *
         * for N from 1 to 28 do
         * 	r := 0 :
         * 	n := N-1 :
         *
         *	for l from iquo(n+3,2) to 70 do
         *		d := 0 :
         *		for s from 0 to n+1 do
         *		 d := d+(-1)^s*binomial(n+1,s)*binomial((s-1)/2,l) :
         *		od :
         *		if 2*l-n > 1 then
         *		r := r+d*(-1)^l*(Zeta(2*l-n) -1) :
         *		fi :
         *	od :
         *	print(evalf((-1)^n*2*r)) ;
         *od :
         *quit :
         */
        static long double Kncoe[] = { .30459198558715155634315638246624251L,
            .72037977439182833573548891941219706L, -.12454959243861367729528855995001087L,
            .27769457331927827002810119567456810e-1L, -.67762371439822456447373550186163070e-2L,
            .17238755142247705209823876688592170e-2L, -.44817699064252933515310345718960928e-3L,
            .11793660000155572716272710617753373e-3L, -.31253894280980134452125172274246963e-4L,
            .83173997012173283398932708991137488e-5L, -.22191427643780045431149221890172210e-5L,
            .59302266729329346291029599913617915e-6L, -.15863051191470655433559920279603632e-6L,
            .42459203983193603241777510648681429e-7L, -.11369129616951114238848106591780146e-7L,
            .304502217295931698401459168423403510e-8L, -.81568455080753152802915013641723686e-9L,
            .21852324749975455125936715817306383e-9L, -.58546491441689515680751900276454407e-10L,
            .15686348450871204869813586459513648e-10L, -.42029496273143231373796179302482033e-11L,
            .11261435719264907097227520956710754e-11L, -.30174353636860279765375177200637590e-12L,
            .80850955256389526647406571868193768e-13L, -.21663779809421233144009565199997351e-13L,
            .58047634271339391495076374966835526e-14L, -.15553767189204733561108869588173845e-14L,
            .41676108598040807753707828039353330e-15L, -.11167065064221317094734023242188463e-15L } ;
        
        register long double Tn_1 = 1.0L ;	/* T_{n-1}(x), started at n=1 */
        register long double Tn = x-2.0L ;	/* T_{n}(x) , started at n=1 */
        register long double resul = Kncoe[0] + Kncoe[1]*Tn ;
        
        x -= 2.0L ;
        
        for(int n = 2 ; n < sizeof(Kncoe)/sizeof(long double) ;n++)
        {
            const long double Tn1 = 2.0L * x * Tn - Tn_1 ;	/* Chebyshev recursion, Eq. 22.7.4 Abramowitz-Stegun */
            resul += Kncoe[n]*Tn1 ;
            Tn_1 = Tn ;
            Tn = Tn1 ;
        }
        return resul ;
    }
}


lss::DivModel::~DivModel()
{
    if(iniEstTyp=="divStream_whole")
    {
        if(zd_who)
        {
            delete zd_who;
        }
        if(mz_who)
        {
            delete mz_who;
        }
        if(nz_who)
        {
            delete nz_who;
        }
    }
    else if(iniEstTyp=="divStream_time")
    {
        if(alpha_tim)
        {
            delete alpha_tim;
        }
        if(beta_tim)
        {
             delete beta_tim;
        }
        if(theta_tim)
        {
            delete theta_tim;
        }
        if(phi_tim)
        {
            for(int k=0; k<K; k++)
            {
                if(phi_tim[k])
                {
                    delete phi_tim[k];
                }
            }
        }
        if(zd_tim)
        {
            delete zd_tim;
        }
        if(mz_tim)
        {
            delete mz_tim;
        }
        if(nz_tim)
        {
            delete nz_tim;
        }
    }
    else
    {
        
    }
}




















