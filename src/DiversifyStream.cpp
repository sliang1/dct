//
//  DiversifyStream.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/3/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#include "DiversifyStream.h"
#include "QueryRep.h"

lss::DiversifyStream::DiversifyStream()
{
    this->init();
}

void
lss::DiversifyStream::openIndex()
{
    std::string indFil=lemur::api::ParamGetString("index");
    if(indFil.length()<=0)
    {
        std::cout<<"Parameter 'index' is not set."<<std::endl;
        return;
    }
    try
    {
        index_=lemur::api::IndexManager::openIndex(indFil);
    }
    catch (const lemur::api::Exception& e)
    {
        std::cout<<"Can't open index, check parameter 'index'. "<<std::endl;
    }
}

void
lss::DiversifyStream::closeIndex()
{
    if(index_!=NULL)
    {
        delete index_;
    }
}

void
lss::DiversifyStream::init()
{
    this->openIndex();
    lss::QueryRep qryRep = lss::QueryRep();
    qryRep.loadQueries(*index_);
    this->closeIndex();
}










