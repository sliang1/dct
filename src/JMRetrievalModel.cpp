//
//  JMRetrievalModel.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 8/4/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#include <stdio.h>
#include "JMRetrievalModel.h"

void
lss::JMRetrievalModel::init()
{
    this->initSmoothing();
    smoothSupportFile_ = lemur::api::ParamGetString( "smoothSupportFile" );
    topDocNum_ = lemur::api::ParamGetInt( "topDocNum", 100 );
}

void
lss::JMRetrievalModel::scoreCollection( const int& qID, lemur::api::IndexedRealVector& pqd_all )
{
    // init retrieval method
    lemur::retrieval::ArrayAccumulator* accumulator
    = new lemur::retrieval::ArrayAccumulator( index_.docCount() );
    lemur::retrieval::SimpleKLRetMethod* retMethod
    = new lemur::retrieval::SimpleKLRetMethod (index_, smoothSupportFile_, *accumulator );
    
    SimpleKLParameter::DocSmoothParam docParam;
    if ( smoothingMethod_ == "dirichlet" ) {
        docParam.smthMethod = SimpleKLParameter::DIRICHLETPRIOR;
        docParam.DirPrior = smoothingParam_;
    }

    if ( smoothingMethod_ == "jm" ) {
        docParam.smthMethod = SimpleKLParameter::JELINEKMERCER;
        docParam.JMLambda = smoothingParam_;
    }
    docParam.smthStrategy = SimpleKLParameter::INTERPOLATE;
    retMethod->setDocSmoothParam( docParam );
    
    SimpleKLParameter::QueryModelParam qryParam;
    qryParam.adjScoreMethod = SimpleKLParameter::QUERYLIKELIHOOD;
    retMethod->setQueryModelParam( qryParam );
    
    // query as SimpleKLQueryModel
    lemur::retrieval::SimpleKLQueryModel* qModel = queries_.query( index_, qID );
    
    // score documents
    retMethod->scoreCollection( *qModel, pqd_all );
    
    // map log(x) values to exp(log(x)) and normalize
    pqd_all.LogToPosterior();
    
    // sort
    pqd_all.Sort();
    
    // clean up
    delete qModel;
    delete retMethod;
    delete accumulator;
}

lss::QUEDOCSCO_T
lss::JMRetrievalModel::getQueDocSco()
{
    std::string queStr;
    std::string docStr;
    lss::QUEDOCSCO_T qDS;
    lemur::api::DOCID_T docID; //int
    const int queryNum = queries_.queryNum();
    for ( ID_T qID = 0; qID < queryNum; qID++ )
    {
        lemur::api::IndexedRealVector pqd_all; // p(q|d) for all documents
        std::cout<<"scoring collection......"<<std::endl;
        this->scoreCollection( qID, pqd_all );
        std::cout<<"end scoring collection......"<<std::endl;
        if(topDocNum_<pqd_all.size())
        {
            pqd_all.resize(topDocNum_);
        }
        
        queStr=queries_.queryID(qID);
        DOCSCO_T dS;
        for ( int i = 0; i < pqd_all.size(); i++ )
        {
            const double& pqd = pqd_all[i].val;
            docID = pqd_all[i].ind;
            docStr=index_.document(docID);
            dS[docStr]=pqd;
            std::cout<<"*query="<<queStr<<" doc="<<docStr<<" score="<<pqd<<" numberOfTerms="<<index_.docLength(docID)<<" total unique terms="<<index_.termCountUnique()<<" total "<<index_.termCount()<<std::endl;
            this->priDocCon(docStr);
        }
        qDS[queStr]=dS;
    }
    
    return qDS;
}


void
lss::JMRetrievalModel::wriQueDocScoToFil()
{
    std::string str;
    std::string queStr;
    std::string docStr;
    std::string savFolStr;
    lss::QUEDOCSCO_T qDS;
    lemur::api::DOCID_T docID; //int
    std::fstream output;
    const int queryNum = queries_.queryNum();
    const double& MINRANSCO=lemur::api::ParamGetDouble("minRanScoForRec", 0.000001);
    
    savFolStr=lemur::api::ParamGetString("savQueDocScoFol");
    if(savFolStr.length()<=0)
    {
        std::cout<<"The parameter savQueDocScoFol has not been set."<<std::endl;
        return;
    }
    for ( ID_T qID = 0; qID < queryNum; qID++ )
    {
        lemur::api::IndexedRealVector pqd_all; // p(q|d) for all documents
        queStr=queries_.queryID(qID);
        std::cout<<"scoring collection for query: "<<queStr<<"......"<<std::endl;
        this->scoreCollection( qID, pqd_all );
        std::cout<<"end scoring collection......"<<std::endl;
        if(topDocNum_<pqd_all.size())
        {
            pqd_all.resize(topDocNum_);
        }
        
        
        output.clear();
        str=savFolStr+"/"+queStr;
        output.open(str.c_str(), std::ios::out);
        if(!output)
        {
            std::cout<<"Can not write to the file: "<<str<<std::endl;
            return;
        }
        for(int i=0; i<pqd_all.size(); i++)
        {
            const double& pqd=pqd_all[i].val;
            docID=pqd_all[i].ind;
            docStr=index_.document(docID);
            if(docStr.length()==18)
            {
                output<<queStr<<" "<<docStr<<" "<<std::setprecision(10)<<pqd<<std::endl;
            }
            if(pqd<MINRANSCO)
            {
                break;
            }
        }
        output.close();
        
        
        
        /*DOCSCO_T dS;
        for ( int i = 0; i < pqd_all.size(); i++ )
        {
            const double& pqd = pqd_all[i].val;
            docID = pqd_all[i].ind;
            docStr=index_.document(docID);
            dS[docStr]=pqd;
            std::cout<<"*query="<<queStr<<" doc="<<docStr<<" score="<<pqd<<" numberOfTerms="<<index_.docLength(docID)<<" total unique terms="<<index_.termCountUnique()<<" total "<<index_.termCount()<<std::endl;
            this->priDocCon(docStr);
        }
        qDS[queStr]=dS;*/
    }
}









































