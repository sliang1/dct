//
//  MP2.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 10/5/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#include <stdio.h>
#include "DataTypesTwo.h"
#include "MP2.h"

//Lemur
#include "Param.hpp"

void
lss::MP2::processMP(const lss::TWODIM_T docTopArr, const std::vector<double> pTopic, const std::string que, lss::DOCSCO_T docSco)
{
    lss::QUEDOCSCO_T::iterator qDS_it;
    lss::DOCSCO_T dS_S;
    lss::DOCSCO_T dS_R;
    lss::DOCSCO_T docInd; //document-index mapping
    lss::DOCINDDOC_T indDoc; //index-document mapping
    lss::DOCINDDOC_T::iterator indDoc_it;
    lss::DOCSCO_T::iterator docInd_it;
    lss::DOCSCO_T::iterator dS_R_it;
    std::vector<double> v;
    std::vector<double> quotient; //
    std::vector<double> s;
    const int aspects=(int)pTopic.size();
    const int numOfDoc=(int)docSco.size();
    int seaNum=(int)docSco.size(); // number of seats
    double temp=0.0;
    double max=0.0;
    double lambda=0.0;
    double sum=0.0;
    int index_i=0;
    int index_d=0;
    int temp_int=0;
    std::string docStr;
    
    lemur::api::ParamGet("lambdaMP2", lambda, 0.5);
    dS_R=docSco;
    temp_int=0;
    for(docInd_it=docSco.begin(); docInd_it!=docSco.end(); docInd_it++)
    {
        docInd.insert(lss::DOCSCO_PAIR_T(docInd_it->first, temp_int));
        indDoc.insert(lss::DOCINDDOC_PAIR_T(temp_int, docInd_it->first));
        temp_int=temp_int+1;
    }
    
    qDS_it=this->qDS.find(que);
    if(qDS_it!=this->qDS.end())
    {
        std::cout<<que<<" has been processed."<<std::endl;
        return;
    }
    
    for(int t=0; t<aspects; t++)
    {
        temp=0.0;
        /*for(int i=0; i<numOfDoc; i++)
         {
         temp=temp+pTopic[t]*docTopArr[i][t];
         }
         v.push_back(temp);*/
        v.push_back(pTopic[t]);
        s.push_back(0.0);
        //quotient.push_back(temp);
        quotient.push_back(pTopic[t]);
    }
    for(int seat=0; seat<seaNum; seat++)
    {
        max=-1.0;
        for(int i=0; i<aspects; i++)
        {
            quotient[i]=v[i]/(2.0*s[i]+1.0);
            if(quotient[i]>max)
            {
                max=quotient[i];
                index_i=i;
            }
        }//for(int i=0; i<aspects; i++)
        max=-1.0;
        for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        {
            docInd_it=docInd.find(dS_R_it->first);
            temp_int=(int)docInd_it->second;
            sum=0.0;
            for(int t=0; t<aspects; t++)
            {
                if(t!=index_i)
                {
                    sum=sum+quotient[t]*docTopArr[temp_int][t];
                }
            }
            temp=lambda*quotient[index_i]*docTopArr[temp_int][index_i]+(1.0-lambda)*sum;
            if(temp>max)
            {
                index_d=temp_int;
                max=temp;
            }
        }//for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        //std::cout<<"document ID="<<index_d<<std::endl;
        indDoc_it=indDoc.find(index_d);
        dS_S.insert(lss::DOCSCO_PAIR_T(indDoc_it->second, seaNum-seat));
        dS_R.erase(indDoc_it->second);
        for(int t=0; t<aspects; t++)
        {
            s[t]=s[t]+docTopArr[index_d][t];
        }
    }//for(int seat=0; seat<seaNum; seat++)
    this->qDS.insert(lss::QUEDOCSCO_PAIR_T(que, dS_S));
}

lss::DOCSCO_T
lss::MP2::processMP2_tim(lss::STRSCOVEC_T docScoVec, const std::vector<double> pTopic, const std::string que, lss::DOCSCO_T docSco)
{
    lss::QUEDOCSCO_T::iterator qDS_it;
    lss::DOCSCO_T dS_S;
    lss::DOCSCO_T dS_R;
    //lss::DOCSCO_T docInd; //document-index mapping
    lss::DOCINDDOC_T indDoc; //index-document mapping
    //lss::DOCINDDOC_T::iterator indDoc_it;
    //lss::DOCSCO_T::iterator docInd_it;
    lss::DOCSCO_T::iterator dS_R_it;
    lss::DOCSCO_T::iterator dS_it;
    lss::STRSCOVEC_T::iterator dSV_it;
    std::vector<double> v;
    std::vector<double> quotient; //
    std::vector<double> s;
    std::vector<double> pdt;
    const int aspects=(int)pTopic.size();
    const int numOfDoc=(int)docSco.size();
    const int seaNum=(int)docSco.size(); // number of seats
    double temp=0.0;
    double max=0.0;
    double max_nor=0.0; //max normalization score
    double lambda=0.0;
    double sum=0.0;
    int index_i=0;
    int index_d=0;
    //int temp_int=0;
    std::string docStr;
    
    
    for(lss::DOCSCO_T::iterator it=docSco.begin(); it!=docSco.end(); it++)
    {
        if(it->second>max_nor)
        {
            max_nor=it->second;
        }
    }
    for(lss::DOCSCO_T::iterator it=docSco.begin(); it!=docSco.end(); it++)
    {
        it->second=it->second/max_nor;
    }
    
    lemur::api::ParamGet("lambdaMP2", lambda, 0.5);
    dS_R=docSco;
    
    for(int t=0; t<aspects; t++)
    {
        v.push_back(pTopic[t]);
        s.push_back(0.0);
        quotient.push_back(pTopic[t]);
        pdt.push_back(0.0);
    }

    for(int seat=0; seat<seaNum; seat++)
    {
        max=-1.0;
        for(int i=0; i<aspects; i++)
        {
            quotient[i]=v[i]/(2.0*s[i]+1.0);
            if(quotient[i]>max)
            {
                max=quotient[i];
                index_i=i;
            }
        }//for(int i=0; i<aspects; i++)
        max=-1.0;
        
        for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        {
            dSV_it=docScoVec.find(dS_R_it->first);
            docStr=dSV_it->first;
            for(int t=0; t<aspects; t++)
            {
                pdt[t]=dSV_it->second[t]*(1.0/seaNum)/pTopic[t]*dS_R_it->second;
            }
            sum=0.0;
            for(int t=0; t<aspects; t++)
            {
                if(t!=index_i)
                {
                    //sum=sum+quotient[t]*dSV_it->second[t];
                    sum=sum+quotient[t]*pdt[t];
                }
            }
            //temp=lambda*quotient[index_i]*dSV_it->second[index_i]+(1.0-lambda)*sum;
            temp=lambda*quotient[index_i]*pdt[index_i] + (1.0-lambda)*(1.0/(aspects-1.0))*sum;
            if(temp>max)
            {
                docStr=dSV_it->first;
                max=temp;
            }
        }//for(dS_R_it=dS_R.begin(); dS_R_it!=dS_R.end(); dS_R_it++)
        //std::cout<<lambda*quotient[index_i]*pdt[index_i]<<" "<<(1.0-lambda)*(1.0/(aspects-1.0))*sum<<std::endl;
        
        if(max<=0.0)
        {
            dS_it=docSco.find(docStr);
            dS_S.insert(lss::DOCSCO_PAIR_T(docStr, dS_it->second));
            std::cout<<quotient[index_i]<<" "<<pdt[index_i]<<std::endl;
        }
        else
        {
            dS_S.insert(lss::DOCSCO_PAIR_T(docStr, max)); //max????
        }
        
        dS_R.erase(docStr);
        
        sum=0.0;
        dSV_it=docScoVec.find(docStr);
        dS_it=docSco.find(docStr);
        for(int t=0; t<aspects; t++)
        {
            pdt[t]=dSV_it->second[t]*(1.0/seaNum)/pTopic[t]*dS_it->second;
            sum=sum+pdt[t];
        }
        for(int t=0; t<aspects; t++)
        {
            //s[t]=s[t]+docTopArr[index_d][t];
            //dSV_it=docScoVec.find(docStr);
            //s[t]=s[t]+dSV_it->second[t];
            if(sum>0.0)
            {
                s[t]=s[t] + pdt[t]/sum;
            }
            else
            {
                s[t]=s[t];
                std::cout<<dS_it->first<<" "<<dSV_it->second[t]<<" "<<dS_it->second<<" "<<pTopic[t]<<std::endl;
            }
        }
    }//for(int seat=0; seat<seaNum; seat++)
    //this->qDS.insert(lss::QUEDOCSCO_PAIR_T(que, dS_S));
    return dS_S;
}


void
lss::MP2::retMod_tim() // the structure of the data must be qID/date/filNam, for time aware diversification in short text stream
{
    std::string str;
    std::string str2;
    //std::string mrrDir;
    std::string mp2Dir;
    std::string queStr;
    std::string datStr;
    std::string filStr;
    std::string patStr;
    std::string fulFilNam; // the full fill name
    //std::string mrrResDir;
    std::string mp2ResDir;
    std::string cmdStr;
    std::string queDocFil, thetaFil;
    std::string queDocDir;
    std::string rankFil;
    //std::string mrrRankFilNam;
    std::string mp2RankFilNam;
    std::string docStr;
    std::string alphaFil_pat;
    std::string alphaFilNam;
    std::string alphaFil;
    unsigned long first;
    unsigned long second;
    string::size_type idx;
    std::fstream input;
    std::fstream input2;
    std::fstream theta_input;
    std::fstream alpha_input;
    lss::Processing proc(index_);
    //lss::Processing proc2;
    lss::DOCSCO_T dS;
    lss::DOCSCO_T::iterator it;
    
    double dou_temp;
    //lss::TWODIM_T docTopArr;
    lss::STRSCOVEC_T docScoVec;
    std::vector<double> pTopic;
    std::vector<std::string> tokens;
    std::vector<double> douVec;
    std::stringstream ss;
    
    mp2Dir=lemur::api::ParamGetString("other_mp2Dir", "");
    mp2ResDir=lemur::api::ParamGetString("mp2ResDir", "");
    queDocDir=lemur::api::ParamGetString("queDocDir", "");
    mp2RankFilNam=lemur::api::ParamGetString("mp2RankFil", "");
    //cmdStr="rm -r "+mrrResDir+"/*";
    std::system(cmdStr.c_str());
    lemur::api::ParamGet("topKMP2", this->topK_, 50);
    proc.itePriFil(mp2Dir);
    for(unsigned long i=0; i<proc.filVec.size(); i++)
    {
        lss::DOCSCO_T dS_res;
        fulFilNam=proc.filVec[i];
        filStr=proc.getFilNam(fulFilNam);
        patStr=proc.getPat(fulFilNam);
        first=filStr.find(".theta");
        if(first>=filStr.length())
        {
            continue;
        }
        datStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        queStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        //std::cout<<proc.filVec[i]<<std::endl;
        //std::cout<<"query="<<queStr<<" patStr="<<datStr<<std::endl;
        str=mp2ResDir+"/"+queStr;
        input.open(str.c_str(), std::ios::in);
        if(!input)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input.close();
        }
        str=mp2ResDir+"/"+queStr+"/"+datStr;
        rankFil=str+"/"+datStr+"."+mp2RankFilNam;
        input2.open(str.c_str(), std::ios::in);
        if(!input2)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input2.close();
        }
        queDocFil=queDocDir+"/"+queStr;
        thetaFil=fulFilNam;
        //std::cout<<"forQueFil="<<queDocFil<<"   thetaFil="<<thetaFil<<std::endl;
        dS=proc.getDocSco(queDocFil, thetaFil, topK_);
        //std::cout<<"queDocFil="<<queDocFil<<" thetaFil="<<thetaFil<<std::endl;
        //this->processMRR(queStr, dS);
        
        docScoVec.clear();
        //std::cout<<"theta file: "<<fulFilNam<<std::endl;
        theta_input.open(fulFilNam.c_str(), std::ios::in);
        if(!theta_input)
        {
            std::cout<<"**Can not open the file: "<<fulFilNam<<std::endl;
            return;
        }
        while(theta_input.eof()!=true)
        {
            std::getline(theta_input, str);
            if(str.length()<=0)
            {
                continue;
            }
            std::istringstream iss(str);
            tokens.clear();
            std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
            douVec.clear();
            for(unsigned long i=0; i<tokens.size(); i++)
            {
                if(i==0)
                {
                    docStr=tokens[0];
                }
                else
                {
                    //ss.clear();
                    //ss<<tokens[i];
                    //ss>>dou_temp;
                    dou_temp=std::atof(tokens[i].c_str());
                    douVec.push_back(dou_temp);
                }
            }
            docScoVec[docStr]=douVec;
        }
        //std::cout<<fulFilNam<<std::endl;
        
        pTopic.clear();
        alphaFil_pat=proc.getPat(fulFilNam);
        str2=proc.getFilNam(fulFilNam);
        second=str2.find('.');
        alphaFilNam=str2.substr(0, second)+".alpha";
        alphaFil=alphaFil_pat+"/"+alphaFilNam;
        alpha_input.open(alphaFil.c_str(), std::ios::in);
        while(alpha_input.eof()!=true) //only one line in the alpha file
        {
            std::getline(alpha_input, str);
            if(str.length()<=0)
            {
                continue;
            }
            std::istringstream iss(str);
            tokens.clear();
            std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
            for(unsigned long i=0; i<tokens.size(); i++)
            {
                //ss.clear();
                //ss<<tokens[i];
                //ss>>dou_temp;
                dou_temp=std::atof(tokens[i].c_str());
                pTopic.push_back(dou_temp);
            }
        } //while(alpha_input.eof()!=true)
        
        alpha_input.close();
        theta_input.close();
        std::cout<<alphaFil<<std::endl;
        //std::cout<<fulFilNam<<std::endl;
        dS_res=this->processMP2_tim(docScoVec, pTopic, queStr, dS);
        ///dS_res=this->processMRR_wor(queStr, dS);
        ///resWri_.writeRank(queStr, dS_res, rankFil, mrrRankFilNam);
        resWri_.writeRank(queStr, dS_res, rankFil, mp2RankFilNam);
    } // for(unsigned long i=0;
    
}

void
lss::MP2::retMod_who() // the structure of the data must be qID/date/filNam, for kdd 2014 in short text stream
{
    std::string str;
    std::string str2;
    //std::string mrrDir;
    std::string mp2Dir;
    std::string mp2Dir2;
    std::string queStr;
    std::string datStr;
    std::string filStr;
    std::string patStr;
    std::string fulFilNam; // the full fill name
    //std::string mrrResDir;
    std::string mp2ResDir;
    std::string cmdStr;
    std::string queDocFil;
    std::string thetaFil; // include up-to-date documents only
    std::string thetaFil_all; // include all the documents
    std::string queDocDir;
    std::string rankFil;
    //std::string mrrRankFilNam;
    std::string mp2RankFilNam;
    std::string docStr;
    std::string alphaFil_pat;
    std::string alphaFilNam;
    std::string alphaFil;
    unsigned long first;
    unsigned long second;
    string::size_type idx;
    std::fstream input;
    std::fstream input2;
    std::fstream theta_input;
    std::fstream theta_all_input;
    std::fstream alpha_input;
    lss::Processing proc(index_);
    //lss::Processing proc2;
    lss::DOCSCO_T dS;
    lss::DOCSCO_T::iterator it;
    
    int ntopics;
    double alpha;
    double dou_temp;
    //lss::TWODIM_T docTopArr;
    lss::STRSCOVEC_T docScoVec;
    lss::STRSCOVEC_T docScoVec_all;
    lss::STRSCOVEC_T::iterator dSV_all_it;
    std::vector<double> pTopic;
    std::vector<std::string> tokens;
    std::vector<double> douVec;
    std::stringstream ss;
    
    mp2Dir=lemur::api::ParamGetString("other_mp2Dir", "");
    mp2Dir2=lemur::api::ParamGetString("other_mp2Dir_2", "");
    mp2ResDir=lemur::api::ParamGetString("mp2ResDir", "");
    queDocDir=lemur::api::ParamGetString("queDocDir", "");
    mp2RankFilNam=lemur::api::ParamGetString("mp2RankFil", "");
    //cmdStr="rm -r "+mrrResDir+"/*";
    //std::system(cmdStr.c_str());
    lemur::api::ParamGet("topKMP2", this->topK_, 50);
    proc.itePriFil(mp2Dir);
    
    for(unsigned long i=0; i<proc.filVec.size(); i++)
    {
        lss::DOCSCO_T dS_res;
        fulFilNam=proc.filVec[i];
        filStr=proc.getFilNam(fulFilNam);
    
        patStr=proc.getPat(fulFilNam);
        first=filStr.find(".theta");
        if(first>=filStr.length())
        {
            continue;
        }
        datStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        queStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        //std::cout<<proc.filVec[i]<<std::endl;
        //std::cout<<"query="<<queStr<<" patStr="<<datStr<<std::endl;
        str=mp2ResDir+"/"+queStr;
        input.open(str.c_str(), std::ios::in);
        if(!input)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input.close();
        }
        str=mp2ResDir+"/"+queStr+"/"+datStr;
        rankFil=str+"/"+datStr+"."+mp2RankFilNam;
        input2.open(str.c_str(), std::ios::in);
        if(!input2)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input2.close();
        }
        
        queDocFil=queDocDir+"/"+queStr;
        thetaFil_all=mp2Dir2+"/"+queStr+"/"+filStr;
        thetaFil=fulFilNam;
        
        //std::cout<<"forQueFil="<<queDocFil<<"   thetaFil="<<thetaFil<<std::endl;
        dS=proc.getDocSco(queDocFil, thetaFil, topK_);
        //std::cout<<"queDocFil="<<queDocFil<<" thetaFil="<<thetaFil<<std::endl;
        //this->processMRR(queStr, dS);
        
        docScoVec.clear();
        //std::cout<<"theta file: "<<fulFilNam<<std::endl;
        theta_input.open(fulFilNam.c_str(), std::ios::in);
        if(!theta_input)
        {
            std::cout<<"Can not open the file: "<<fulFilNam<<std::endl;
            return;
        }
        theta_all_input.open(thetaFil_all.c_str(), std::ios::in);
        if(!theta_all_input)
        {
            std::cout<<"*Can not open the file: "<<thetaFil_all<<std::endl;
            return;
        }
        
        while(theta_all_input.eof()!=true)
        {
            std::getline(theta_all_input, str);
            if(str.length()<=0)
            {
                continue;
            }
            std::istringstream iss(str);
            tokens.clear();
            std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
            douVec.clear();
            for(unsigned long i=0; i<tokens.size(); i++)
            {
                if(i==0)
                {
                    docStr=tokens[0];
                }
                else
                {
                    dou_temp=std::atof(tokens[i].c_str());
                    douVec.push_back(dou_temp);
                }
            }
            docScoVec_all[docStr]=douVec;
        } //while(theta_all_input.eof()!=true)
        
        while(theta_input.eof()!=true)
        {
            std::getline(theta_input, str);
            if(str.length()<=0)
            {
                continue;
            }
            std::istringstream iss(str);
            tokens.clear();
            std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
            douVec.clear();
            docStr=tokens[0];
            dSV_all_it=docScoVec_all.find(docStr);
            if(dSV_all_it!=docScoVec_all.end())
            {
                douVec=dSV_all_it->second;
            }
            else
            {
                continue;
            }
            /*for(unsigned long i=0; i<tokens.size(); i++)
            {
                if(i==0)
                {
                    docStr=tokens[0];
                }
                else
                {
                    //ss.clear();
                    //ss<<tokens[i];
                    //ss>>dou_temp;
                    dou_temp=std::atof(tokens[i].c_str());
                    douVec.push_back(dou_temp);
                }
            }*/
            docScoVec[docStr]=douVec;
        }
        //std::cout<<fulFilNam<<std::endl;
        pTopic.clear();
        lemur::api::ParamGet("ntopics", ntopics, 10);
        lemur::api::ParamGet("alpha", alpha, 1.0);
        for(int i=0; i<ntopics; i++)
        {
            pTopic.push_back(alpha/(alpha*ntopics));
        }
        theta_input.close();
        theta_all_input.close();
        dS_res=this->processMP2_tim(docScoVec, pTopic, queStr, dS);
        resWri_.writeRank(queStr, dS_res, rankFil, mp2RankFilNam);
        
        /*pTopic.clear();
        alphaFil_pat=proc.getPat(fulFilNam);
        str2=proc.getFilNam(fulFilNam);
        second=str2.find('.');
        alphaFilNam=str2.substr(0, second)+".alpha";
        alphaFil=alphaFil_pat+"/"+alphaFilNam;
        alpha_input.open(alphaFil.c_str(), std::ios::in);
        while(alpha_input.eof()!=true) //only one line in the alpha file
        {
            std::getline(alpha_input, str);
            if(str.length()<=0)
            {
                continue;
            }
            std::istringstream iss(str);
            tokens.clear();
            std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
            for(unsigned long i=0; i<tokens.size(); i++)
            {
                //ss.clear();
                //ss<<tokens[i];
                //ss>>dou_temp;
                dou_temp=std::atof(tokens[i].c_str());
                pTopic.push_back(dou_temp);
            }
        } //while(alpha_input.eof()!=true)
        
        alpha_input.close();
        theta_input.close();
        std::cout<<alphaFil<<std::endl;*/
        //std::cout<<fulFilNam<<std::endl;
        //dS_res=this->processMP2_tim(docScoVec, pTopic, queStr, dS);
        //resWri_.writeRank(queStr, dS_res, rankFil, mp2RankFilNam);
    } // for(unsigned long i=0;
    
}

void
lss::MP2::retMod_lan() // the structure of the data must be qID/date/filNam, for kdd 2014 in short text stream
{
    std::string str;
    std::string str2;
    //std::string mrrDir;
    std::string mp2Dir;
    //std::string mp2Dir2;
    std::string queStr;
    std::string datStr;
    std::string filStr;
    std::string patStr;
    std::string fulFilNam; // the full fill name
    //std::string mrrResDir;
    std::string mp2ResDir;
    std::string cmdStr;
    std::string queDocFil;
    std::string thetaFil; // include up-to-date documents only
    std::string thetaFil_all; // include all the documents
    std::string queDocDir;
    std::string rankFil;
    //std::string mrrRankFilNam;
    std::string mp2RankFilNam;
    std::string docStr;
    std::string alphaFil_pat;
    std::string alphaFilNam;
    std::string alphaFil;
    unsigned long first;
    unsigned long second;
    string::size_type idx;
    std::fstream input;
    std::fstream input2;
    std::fstream theta_input;
    std::fstream theta_all_input;
    std::fstream alpha_input;
    lss::Processing proc(index_);
    //lss::Processing proc2;
    lss::DOCSCO_T dS;
    lss::DOCSCO_T::iterator it;
    
    int ntopics;
    double alpha;
    double dou_temp;
    //lss::TWODIM_T docTopArr;
    lss::STRSCOVEC_T docScoVec;
    lss::STRSCOVEC_T docScoVec_all;
    lss::STRSCOVEC_T::iterator dSV_all_it;
    std::vector<double> pTopic;
    std::vector<std::string> tokens;
    std::vector<double> douVec;
    std::stringstream ss;
    
    mp2Dir=lemur::api::ParamGetString("other_mp2Dir", "");
    //mp2Dir2=lemur::api::ParamGetString("other_mp2Dir_2", "");
    mp2ResDir=lemur::api::ParamGetString("mp2ResDir", "");
    queDocDir=lemur::api::ParamGetString("queDocDir", "");
    mp2RankFilNam=lemur::api::ParamGetString("mp2RankFil", "");
    //cmdStr="rm -r "+mrrResDir+"/*";
    //std::system(cmdStr.c_str());
    lemur::api::ParamGet("topKMP2", this->topK_, 50);
    proc.itePriFil(mp2Dir);
    
    for(unsigned long i=0; i<proc.filVec.size(); i++)
    {
        lss::DOCSCO_T dS_res;
        fulFilNam=proc.filVec[i];
        filStr=proc.getFilNam(fulFilNam);
        
        patStr=proc.getPat(fulFilNam);
        first=filStr.find(".theta");
        if(first>=filStr.length())
        {
            continue;
        }
        datStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        queStr=proc.getFilNam(patStr);
        patStr=proc.getPat(patStr);
        
        str=mp2ResDir+"/"+queStr;
        input.open(str.c_str(), std::ios::in);
        if(!input)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input.close();
        }
        str=mp2ResDir+"/"+queStr+"/"+datStr;
        rankFil=str+"/"+datStr+"."+mp2RankFilNam;
        input2.open(str.c_str(), std::ios::in);
        if(!input2)
        {
            cmdStr="mkdir "+str;
            std::system(cmdStr.c_str());
        }
        else
        {
            input2.close();
        }
        
        queDocFil=queDocDir+"/"+queStr;
        //thetaFil_all=mp2Dir2+"/"+queStr+"/"+filStr;
        thetaFil=fulFilNam;
        std::cout<<"thetaFil="<<thetaFil<<std::endl;
        dS_res.clear();
        
        dS=proc.getDocSco(queDocFil, thetaFil, topK_);
        
        resWri_.writeRank(queStr, dS, rankFil, mp2RankFilNam);
        
        /*docScoVec.clear();
        //std::cout<<"theta file: "<<fulFilNam<<std::endl;
        theta_input.open(fulFilNam.c_str(), std::ios::in);
        if(!theta_input)
        {
            std::cout<<"Can not open the file: "<<fulFilNam<<std::endl;
            return;
        }
        theta_all_input.open(thetaFil_all.c_str(), std::ios::in);
        if(!theta_all_input)
        {
            std::cout<<"*Can not open the file: "<<thetaFil_all<<std::endl;
            return;
        }
        
        while(theta_all_input.eof()!=true)
        {
            std::getline(theta_all_input, str);
            if(str.length()<=0)
            {
                continue;
            }
            std::istringstream iss(str);
            tokens.clear();
            std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
            douVec.clear();
            for(unsigned long i=0; i<tokens.size(); i++)
            {
                if(i==0)
                {
                    docStr=tokens[0];
                }
                else
                {
                    dou_temp=std::atof(tokens[i].c_str());
                    douVec.push_back(dou_temp);
                }
            }
            docScoVec_all[docStr]=douVec;
        } //while(theta_all_input.eof()!=true)
        
        while(theta_input.eof()!=true)
        {
            std::getline(theta_input, str);
            if(str.length()<=0)
            {
                continue;
            }
            std::istringstream iss(str);
            tokens.clear();
            std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(tokens));
            douVec.clear();
            docStr=tokens[0];
            dSV_all_it=docScoVec_all.find(docStr);
            if(dSV_all_it!=docScoVec_all.end())
            {
                douVec=dSV_all_it->second;
            }
            else
            {
                continue;
            }
            docScoVec[docStr]=douVec;
        }
        //std::cout<<fulFilNam<<std::endl;
        pTopic.clear();
        lemur::api::ParamGet("ntopics", ntopics, 10);
        lemur::api::ParamGet("alpha", alpha, 1.0);
        for(int i=0; i<ntopics; i++)
        {
            pTopic.push_back(alpha/(alpha*ntopics));
        }
        theta_input.close();
        theta_all_input.close();
        dS_res=this->processMP2_tim(docScoVec, pTopic, queStr, dS);
        resWri_.writeRank(queStr, dS_res, rankFil, mp2RankFilNam);*/
    } // for(unsigned long i=0;
    
}

lss::QUEDOCSCO_T
lss::MP2::getQDS()
{
    return this->qDS;
}













