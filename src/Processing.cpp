//
//  Processing.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 7/14/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#include "Processing.h"
#include <algorithm>
#include "dirent.h"
#include <cstddef>

/*lss::Processing::Processing()
{
    //this->getSubSetDat();
    //this->getSubSetDatRemRet();
    //this->mvFilIntGro();
    //this->extHasTag();
    //this->extHasMuiTag();
    //this->proRemInvHasTag();
    //this->finQue();
    //this->finQue2();
}*/

void
lss::Processing::getSubSetDat()
{
    int index;
    bool theFirWri;
    std::string str;
    std::string outputStr;
    std::string resStr;
    
    std::string docNoStr;
    std::string datStr;
    std::string retStr;
    std::string folStr;
    std::string friStr;
    std::string texStr;
    std::string hasStr;
    std::string urlStr;
    std::string lanStr;
    
    std::string filNamStr;
    std::string empHasStr="<HASHTAGS>   </HASHTAGS>";
    std::vector<std::string> filVec;
    std::fstream input;
    std::fstream output;
    DIR *dir;
    struct dirent *ent;
    
    std::string pathStr=lemur::api::ParamGetString("path");
    filVec.clear();
    if((dir = opendir (pathStr.c_str())) != NULL)
    {
        while((ent = readdir (dir)) != NULL)
        {
            if(ent->d_name[0]!='.')
            {
                filVec.push_back(ent->d_name);
            }
        }
        closedir(dir);
    }
    else
    {
        std::cout<<"Can not find the folder: "<<pathStr<<std::endl;
        return;
    }
    index=0;
    for(unsigned int i=0; i<filVec.size(); i++)
    {
        filNamStr=pathStr+"/"+filVec[i];
        input.open(filNamStr.c_str(), std::ios::in);
        theFirWri=true;
        while(input.eof()!=true)
        {
            std::getline(input, str);
            if(str.length()<=0)
            {
                continue;
            }
            index=index+1;
            if(str.length()==5) //str is <DOC>
            {
                index=1;
            }
            switch(index)
            {
                case 2:
                    docNoStr=str;
                    break;
                case 3:
                    datStr=str;
                    break;
                case 4:
                    retStr=str;
                    break;
                case 5:
                    folStr=str;
                    break;
                case 6:
                    friStr=str;
                    break;
                case 7:
                    texStr=str;
                    break;
                case 8:
                    hasStr=str;
                    break;
                case 9:
                    urlStr=str;
                    break;
                case 10:
                    lanStr=str;
            }//switch(index)
            if(index==10)
            {
                if(empHasStr.length()==hasStr.length() || lanStr!="<LANG> en </LANG>")
                {
                    continue;
                }
                if(theFirWri==true)
                {
                    outputStr=lemur::api::ParamGetString("outPath");
                    outputStr=outputStr+"/"+filVec[i];
                    output.open(outputStr.c_str(), std::ios::out);
                    theFirWri=false;
                }
                else
                {
                    resStr="<DOC>\n"+docNoStr+"\n"+datStr+"\n"+retStr+"\n"+folStr+"\n"+friStr+"\n"+texStr+"\n"+hasStr+"\n"+urlStr+"\n"+lanStr+"\n"+"</DOC>\n";
                    output<<resStr<<std::endl;
                }
            }
            
        } //while(input.eof()!=true)
        std::cout<<filVec[i]<<std::endl;
        input.close();
        output.close();
    } //for(unsigned int i=0;
}


void
lss::Processing::mvFilIntGro()
{
    unsigned long first;
    unsigned long second;
    std::string str;
    std::string str2;
    std::string str3;
    std::string str4;
    std::string subStr;
    std::string rooPatStr;
    std::string folStr;
    std::stringstream ss;
    std::vector<std::string> mooVec;
    std::vector<std::string> mooSubVec;
    std::vector<std::string> filVec;
    std::vector<std::string> dayVec;
    std::vector<std::string>::iterator it;
    
    rooPatStr=lemur::api::ParamGetString("rootData");
    if(rooPatStr.length()<=0)
    {
        std::cout<<"The parameter rootData has not been set."<<std::endl;
        return;
    }
    DIR *dir;
    struct dirent *ent;
    mooVec.clear();
    if((dir = opendir(rooPatStr.c_str())) != NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            if(ent->d_name[0]!='.')
            {
                mooVec.push_back(ent->d_name);
            }
        }
    }//if((dir = opendir
    closedir(dir);
    mooSubVec.clear();
    for(unsigned long i=1; i<=4; i++)
    {
        ss.clear();
        ss<<i;
        ss>>str;
        mooSubVec.push_back(str);
    }
    
    for(unsigned long i=0; i<mooVec.size(); i++)
    {
        for(unsigned long j=0; j<mooSubVec.size(); j++)
        {
            folStr=rooPatStr+"/"+mooVec[i]+"/"+mooSubVec[j];
            filVec.clear();
            if((dir = opendir(folStr.c_str())) != NULL)
            {
                while((ent = readdir(dir)) != NULL)
                {
                    if(ent->d_name[0]!='.')
                    {
                        filVec.push_back(ent->d_name);
                    }
                }
            } //if((dir = opendir
            dayVec.clear();
            for(unsigned long k=0; k<filVec.size(); k++)
            {
                str=filVec[k];
                first=str.find("T", 0);
                second=str.rfind('-', str.length()-1);
                subStr=str.substr(second+1, first-second-1);
                it=std::find(dayVec.begin(), dayVec.end(), subStr);
                if(it == dayVec.end())
                {
                    dayVec.push_back(subStr);
                    str2="mkdir "+rooPatStr+"/"+mooVec[i]+"/"+mooSubVec[j]+"/"+subStr;
                    std::system(str2.c_str());
                }
            } //for(unsigned long k=0;
            for(unsigned long k=0; k<filVec.size(); k++)
            {
                str=filVec[k];
                first=str.find("T", 0);
                second=str.rfind('-', str.length()-1);
                subStr=str.substr(second+1, first-second-1);
                str4=rooPatStr+"/"+mooVec[i]+"/"+mooSubVec[j];
                str3="mv  "+ str4+"/"+str+"   "+str4+"/"+subStr;
                std::system(str3.c_str());
            }
        } //for(unsigned long j=0;
    } //for(unsigned long i=0;
}

void
lss::Processing::extHasTag()
{
    std::string str;
    std::string str1;
    std::string str2;
    std::string comStr;
    std::string rooStr;
    std::string monStr;
    std::string subStr;
    std::string subStr2;
    std::string dayStr;
    std::string filStr;
    std::string inputStr;
    std::string outputStr;
    std::string hasPatStr;
    std::string word;
    unsigned long first;
    unsigned long second;
    std::fstream input;
    std::fstream output;
    DIR *dirMon;
    DIR *dirSub;
    DIR *dirDay;
    DIR *dirFil;
    struct dirent *entMon;
    struct dirent *entSub;
    struct dirent *entDay;
    struct dirent *entFil;
    
    rooStr=lemur::api::ParamGetString("rootData");
    if(rooStr.length()<=0)
    {
        std::cout<<"The paramenter hashtagExtPath has not been set."<<std::endl;
    }
    hasPatStr=lemur::api::ParamGetString("hashtagExtPath");
    if(hasPatStr.length()<=0)
    {
        std::cout<<"The parameter rootData has not been set."<<std::endl;
    }
    if((dirMon = opendir(rooStr.c_str())) != NULL)
    {
        while((entMon = readdir(dirMon)) != NULL)
        {
            if(entMon->d_name[0]!='.')
            {
                str=entMon->d_name;
                monStr=rooStr+"/"+str;
                std::cout<<"monStr="<<monStr<<std::endl;
                if((dirSub = opendir(monStr.c_str())) != NULL)
                {
                    while((entSub = readdir(dirSub)) != NULL)
                    {
                        if(entSub->d_name[0]!='.')
                        {
                            str=entSub->d_name;
                            subStr=monStr+"/"+str;
                            std::cout<<"subStr="<<subStr<<std::endl;
                            if((dirDay = opendir(subStr.c_str())) != NULL)
                            {
                                while((entDay = readdir(dirDay)) != NULL)
                                {
                                    if(entDay->d_name[0]!='.')
                                    {
                                        str=entDay->d_name;
                                        filStr=subStr+"/"+str;
                                        outputStr=hasPatStr+"/"+entMon->d_name+"-"+entDay->d_name;
                                        output.open(outputStr.c_str(), std::ios::out);
                                        std::cout<<"**** "<<entMon->d_name<<"  "<<entSub->d_name<<"  "<<entDay->d_name<<" ++++"<<filStr<<std::endl;
                                        if((dirFil = opendir(filStr.c_str())) != NULL)
                                        {
                                            while((entFil = readdir(dirFil)) != NULL)
                                            {
                                                if(entFil->d_name[0]!='.')
                                                {
                                                    str=entFil->d_name;
                                                    inputStr=filStr+"/"+str;
                                                    input.open(inputStr.c_str(), std::ios::in);
                                                    while(input.eof()!=true)
                                                    {
                                                        std::getline(input, str);
                                                        first=str.find("<HASHTAGS>");
                                                        if(first!=0)
                                                        {
                                                            continue;
                                                        }
                                                        subStr2=str.substr(11, str.length()-22);
                                                        std::istringstream iss(subStr2);
                                                        while(iss>>word)
                                                        {
                                                            output<<word<<std::endl;
                                                        }
                                                    }
                                                    input.close();
                                                }
                                            }
                                            //output.close();
                                            closedir(dirFil);
                                        } //if((dirFil = opendir(filStr.
                                        output.close();
                                    }
                                } //while((entDay = readdir(dirDay)
                            } //if((dirDay = opendir(subStr.
                            closedir(dirDay);
                        }
                    } //while((entSub = readdir(dirSub)
                } //if((dirSub = opendir(str.
                closedir(dirSub);
            }
        } //while((entMon = readdir(dirMon))
    } //if((dirMon = opendir(rooStr.
    closedir(dirMon);
    
    
    
    /*if((dir = opendir(rooPatStr.c_str())) != NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            if(ent->d_name[0]!='.')
            {
                mooVec.push_back(ent->d_name);
            }
        }
    }//if((dir = opendir
    closedir(dir); */
}

void
lss::Processing::extHasMuiTag()
{
    std::string str;
    std::string str1;
    std::string str2;
    std::string comStr;
    std::string rooStr;
    std::string monStr;
    std::string subStr;
    std::string subStr2;
    std::string dayStr;
    std::string filStr;
    std::string inputStr;
    std::string outputStr;
    std::string hasPatStr;
    std::string word;
    unsigned long first;
    unsigned long second;
    std::vector<std::string> strVec;
    std::fstream input;
    std::fstream output;
    DIR *dirMon;
    DIR *dirSub;
    DIR *dirDay;
    DIR *dirFil;
    struct dirent *entMon;
    struct dirent *entSub;
    struct dirent *entDay;
    struct dirent *entFil;
    
    rooStr=lemur::api::ParamGetString("rootData");
    if(rooStr.length()<=0)
    {
        std::cout<<"The paramenter hashtagExtPath has not been set."<<std::endl;
    }
    hasPatStr=lemur::api::ParamGetString("hashtagExtPath");
    if(hasPatStr.length()<=0)
    {
        std::cout<<"The parameter rootData has not been set."<<std::endl;
    }
    if((dirMon = opendir(rooStr.c_str())) != NULL)
    {
        while((entMon = readdir(dirMon)) != NULL)
        {
            if(entMon->d_name[0]!='.')
            {
                str=entMon->d_name;
                monStr=rooStr+"/"+str;
                std::cout<<"monStr="<<monStr<<std::endl;
                if((dirSub = opendir(monStr.c_str())) != NULL)
                {
                    while((entSub = readdir(dirSub)) != NULL)
                    {
                        if(entSub->d_name[0]!='.')
                        {
                            str=entSub->d_name;
                            subStr=monStr+"/"+str;
                            std::cout<<"subStr="<<subStr<<std::endl;
                            if((dirDay = opendir(subStr.c_str())) != NULL)
                            {
                                while((entDay = readdir(dirDay)) != NULL)
                                {
                                    if(entDay->d_name[0]!='.')
                                    {
                                        str=entDay->d_name;
                                        filStr=subStr+"/"+str;
                                        outputStr=hasPatStr+"/"+entMon->d_name+"-"+entDay->d_name;
                                        output.open(outputStr.c_str(), std::ios::out);
                                        std::cout<<"**** "<<entMon->d_name<<"  "<<entSub->d_name<<"  "<<entDay->d_name<<" ++++"<<filStr<<std::endl;
                                        if((dirFil = opendir(filStr.c_str())) != NULL)
                                        {
                                            while((entFil = readdir(dirFil)) != NULL)
                                            {
                                                if(entFil->d_name[0]!='.')
                                                {
                                                    str=entFil->d_name;
                                                    inputStr=filStr+"/"+str;
                                                    input.open(inputStr.c_str(), std::ios::in);
                                                    while(input.eof()!=true)
                                                    {
                                                        std::getline(input, str);
                                                        first=str.find("<HASHTAGS>");
                                                        if(first!=0)
                                                        {
                                                            continue;
                                                        }
                                                        subStr2=str.substr(11, str.length()-22);
                                                        std::istringstream iss(subStr2);
                                                        strVec.clear();
                                                        while(iss>>word)
                                                        {
                                                            strVec.push_back(word);
                                                            //output<<word<<std::endl;
                                                        }
                                                        if(strVec.size()>1)
                                                        {
                                                            for(unsigned long i=0; i<strVec.size(); i++)
                                                            {
                                                                output<<strVec[i]<<" ";
                                                            }
                                                            output<<std::endl;
                                                        }
                                                    }
                                                    input.close();
                                                }
                                            }
                                            //output.close();
                                            closedir(dirFil);
                                        } //if((dirFil = opendir(filStr.
                                        output.close();
                                    }
                                } //while((entDay = readdir(dirDay)
                            } //if((dirDay = opendir(subStr.
                            closedir(dirDay);
                        }
                    } //while((entSub = readdir(dirSub)
                } //if((dirSub = opendir(str.
                closedir(dirSub);
            }
        } //while((entMon = readdir(dirMon))
    } //if((dirMon = opendir(rooStr.
    closedir(dirMon);
}

void
lss::Processing::proRemInvHasTag()
{
    std::string str;
    std::string inputStr;
    std::string outputStr;
    unsigned long i=0;
    unsigned long j=0;
    std::fstream input;
    std::fstream output;
    
    inputStr=lemur::api::ParamGetString("remHasFil");
    if(inputStr.length()<=0)
    {
        std::cout<<"The parameter remHasFil is not set."<<std::endl;
        return;
    }
    outputStr=lemur::api::ParamGetString("remHasSavFil");
    if(outputStr.length()<=0)
    {
        std::cout<<"The parameter remHasSavFil is not set."<<std::endl;
        return;
    }
    input.open(inputStr.c_str(), std::ios::in);
    output.open(outputStr.c_str(), std::ios::out);
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=1)
        {
            continue;
        }
        for(i=0; i<str.size(); i++)
        {
            if( (str[i]>='a' && str[i]<='z') || (str[i]>='A' && str[i]<='Z') )
            {
                continue;
            }
            else
            {
                break;
            }
        } //for(i=0; i<str.size(); i++)
        if(i==str.size() && str.length()>=2)
        {
            for(j=1; j<str.length(); j++)
            {
                if(str[j-1]==str[j])
                {
                    break;
                }
            }
            if(j==str.length() && str.length()<19)
            {
                output<<str<<std::endl;
            }
            else
            {
                std::cout<<str<<std::endl;
            }
        }
    }
}


void
lss::Processing::getSubSetDatRemRet()
{
    int index;
    bool theFirWri;
    unsigned long first;
    std::string str;
    std::string outputStr;
    std::string resStr;
    std::string resStr2;
    
    std::string docNoStr;
    std::string datStr;
    std::string retStr;
    std::string folStr;
    std::string friStr;
    std::string texStr;
    std::string hasStr;
    std::string urlStr;
    std::string lanStr;
    
    std::string filNamStr;
    std::string empHasStr="<HASHTAGS>   </HASHTAGS>";
    std::vector<std::string> filVec;
    std::fstream input;
    std::fstream output;
    DIR *dir;
    struct dirent *ent;
    
    std::string pathStr=lemur::api::ParamGetString("path");
    filVec.clear();
    if((dir = opendir (pathStr.c_str())) != NULL)
    {
        while((ent = readdir (dir)) != NULL)
        {
            if(ent->d_name[0]!='.')
            {
                filVec.push_back(ent->d_name);
            }
        }
        closedir(dir);
    }
    else
    {
        std::cout<<"Can not find the folder: "<<pathStr<<std::endl;
        return;
    }
    index=0;
    for(unsigned int i=0; i<filVec.size(); i++)
    {
        filNamStr=pathStr+"/"+filVec[i];
        input.open(filNamStr.c_str(), std::ios::in);
        theFirWri=true;
        while(input.eof()!=true)
        {
            std::getline(input, str);
            if(str.length()<=0)
            {
                continue;
            }
            index=index+1;
            if(str.length()==5) //str is <DOC>
            {
                index=1;
            }
            switch(index)
            {
                case 2:
                    docNoStr=str;
                    break;
                case 3:
                    datStr=str;
                    break;
                case 4:
                    retStr=str;
                    break;
                case 5:
                    folStr=str;
                    break;
                case 6:
                    friStr=str;
                    break;
                case 7:
                    texStr=str;
                    break;
                case 8:
                    hasStr=str;
                    break;
                case 9:
                    urlStr=str;
                    break;
                case 10:
                    lanStr=str;
            }//switch(index)
            if(index==10)
            {
                if(empHasStr.length()==hasStr.length() || lanStr!="<LANG> en </LANG>")
                {
                    continue;
                }
                if(theFirWri==true)
                {
                    outputStr=lemur::api::ParamGetString("outPath");
                    outputStr=outputStr+"/"+filVec[i];
                    output.open(outputStr.c_str(), std::ios::out);
                    theFirWri=false;
                }
                else
                {
                    first=texStr.find("<TEXT> RT");
                    if(first!=0)
                    {
                        //resStr="<DOC>\n"+docNoStr+"\n"+datStr+"\n"+retStr+"\n"+folStr+"\n"+friStr+"\n"+texStr+"\n"+hasStr+"\n"+urlStr+"\n"+lanStr+"\n"+"</DOC>\n";
                        resStr2="<DOC>\n"+docNoStr+"\n"+texStr+"\n"+"</DOC>\n";
                        output<<resStr2<<std::endl;
                        //output<<resStr<<std::endl;
                    }
                    
                }
            }
            
        } //while(input.eof()!=true)
        std::cout<<filVec[i]<<std::endl;
        input.close();
        output.close();
    } //for(unsigned int i=0;

}


void
lss::Processing::finQue()
{
    int count;
    int countInFil;
    double pro;
    std::string str;
    std::string str2;
    std::string lineStr;
    std::string muiHasPatStr; // trecMuiHasExtRemRetPath
    std::string hasTagFilStr;
    std::string hasTagOutFilStr;
    std::string matStr; //match string
    unsigned long first;
    std::vector<std::string> filVec;
    std::fstream input;
    std::fstream input2;
    std::fstream output;
    DIR *dir;
    struct dirent *ent;
    
    
    muiHasPatStr=lemur::api::ParamGetString("trecMuiHasExtRemRetPath");
    if(muiHasPatStr.length()<=0)
    {
        std::cout<<"The parameter trecMuiHasExtRemRetPath has not been set."<<std::endl;
        return;
    }
    hasTagFilStr=lemur::api::ParamGetString("hasTagFil");
    if(hasTagFilStr.length()<=0)
    {
        std::cout<<"The parameter hasTagFil has not been set."<<std::endl;
        return;
    }
    hasTagOutFilStr=lemur::api::ParamGetString("hasTagOutFil");
    if(hasTagOutFilStr.length()<=0)
    {
        std::cout<<"The parameter hasTagOutFil has not been set."<<std::endl;
        return;
    }
    filVec.clear();
    if((dir=opendir(muiHasPatStr.c_str())) != NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            if(ent->d_name[0] != '.')
            {
                filVec.push_back(ent->d_name);
            }
        }
    }
    closedir(dir);
    std::sort(filVec.begin(), filVec.end());
    input.open(hasTagFilStr.c_str(), std::ios::in);
    output.open(hasTagOutFilStr.c_str(), std::ios::out);
    while(input.eof()!=true)
    {
        std::getline(input, lineStr);
        if(lineStr.length()<=0)
        {
            continue;
        }
        countInFil=0;
        for(unsigned long i=0; i<filVec.size(); i++)
        {
            str2=muiHasPatStr+"/"+filVec[i];
            input2.open(str2.c_str(), std::ios::in);
            count=0;
            while(input2.eof()!=true)
            {
                std::getline(input2, str2);
                if(str2.length()<=0)
                {
                    continue;
                }
                matStr=lineStr+" ";
                first=str2.find(matStr);
                if(first==0)
                {
                    count=count+1;
                    break;
                }
                else if(first<str.length() && str[first-1]==' ')
                {
                    count=count+1;
                    break;
                }
                else
                {
                    
                }
            }
            input2.close();
            if(count>0)
            {
                countInFil=countInFil+1;
            }
        } //for(unsigned long i=0;
        pro=countInFil*1.0/filVec.size();
        if(pro>=0.3)
        {
            std::cout<<lineStr<<" "<<pro<<std::endl;
            output<<lineStr<<" "<<pro<<std::endl;
        }
        //std::cout<<lineStr<<" "<<countInFil*1.0/filVec.size()<<std::endl;
    } //while(input.eof()!=true)
    input.close();
    output.close();
}

void
lss::Processing::finQue2()
{
    int count;
    double filSiz;
    double pro;
    std::string str;
    std::string hasTagFilStr;
    std::string muiHasPatStr;
    std::string inputStr;
    std::string hasTagOutFilStr;
    lss::DOCSCO_T hasCou;
    lss::DOCDOCSCO_T docHasCou; //document-hastag-count
    lss::DOCDOCSCO_T::iterator it;
    lss::DOCSCO_T::iterator hasIt;
    DIR *dir;
    struct dirent *ent;
    std::fstream input;
    std::fstream hasInput;
    std::fstream output;
    
    
    hasTagFilStr=lemur::api::ParamGetString("hasTagFil");
    if(hasTagFilStr.length()<=0)
    {
        std::cout<<"The parameter hasTagFil has not been set."<<std::endl;
        return;
    }
    muiHasPatStr=lemur::api::ParamGetString("trecMuiHasExtRemRetPath");
    if(muiHasPatStr.length()<=0)
    {
        std::cout<<"The parameter trecMuiHasExtRemRetPath has not been set."<<std::endl;
        return;
    }
    input.open(hasTagFilStr.c_str(), std::ios::in);
    if(!input)
    {
        std::cout<<"Can not open the file:"<<hasTagFilStr<<std::endl;
        return;
    }
    hasTagOutFilStr=lemur::api::ParamGetString("hasTagOutFil");
    if(hasTagOutFilStr.length()<=0)
    {
        std::cout<<"The parameter hasTagOutFil has not been set."<<std::endl;
        return;
    }
    output.open(hasTagOutFilStr.c_str(), std::ios::out);
    
    if((dir=opendir(muiHasPatStr.c_str())) != NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            if(ent->d_name[0] != '.')
            {
                hasCou.clear();
                docHasCou.insert(lss::DOCDOCSCO_PAIR_T(ent->d_name, hasCou));
            }
        }
    }
    closedir(dir);
    filSiz=docHasCou.size();
    
    for(it=docHasCou.begin(); it!=docHasCou.end(); it++)
    {
        inputStr=muiHasPatStr+"/"+it->first;
        hasInput.open(inputStr.c_str(), std::ios::in);
        if(!hasInput)
        {
            std::cout<<"Can not open the file: "<<inputStr<<std::endl;
            return;
        }
        while(hasInput.eof()!=true)
        {
            std::getline(hasInput, str);
            if(str.length()<=0)
            {
                continue;
            }
            it->second.insert(lss::DOCSCO_PAIR_T(str, 1.0));
        }
        
        hasInput.close();
        
    } //for(it=docHasCou.begin();
    
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        count=0;
        for(it=docHasCou.begin(); it!=docHasCou.end(); it++)
        {
            hasIt=it->second.find(str);
            if(hasIt==it->second.end())
            {
                continue;
            }
            else
            {
                count=count+1;
            }
        }
        pro=count*1.0/filSiz;
        if(pro>=0.3)
        {
            output<<str<<" "<<pro<<std::endl;
            std::cout<<str<<" "<<pro<<std::endl;
        }
    }
    
    input.close();
    output.close();
}

void
lss::Processing::forQue()
{
    int count;
    std::string str;
    std::string countStr;
    std::string resStr;
    std::string inputStr;
    std::string outputStr;
    std::fstream input;
    std::fstream output;
    std::stringstream ss;
    
    inputStr=lemur::api::ParamGetString("forQueInput");
    if(inputStr.length()<=0)
    {
        std::cout<<"The parameter forQueInput has not been set."<<std::endl;
        return;
    }
    outputStr=lemur::api::ParamGetString("forQueOutput");
    if(outputStr.length()<=0)
    {
        std::cout<<"The parameter forQueOutput has not been set."<<std::endl;
        return;
    }
    input.open(inputStr.c_str(), std::ios::in);
    output.open(outputStr.c_str(), std::ios::out);
    count=0;
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        count++;
        ss.clear();
        ss<<count;
        ss>>countStr;
        if(count<10)
        {
            countStr="q-00"+countStr;
        }
        else if(count>=10 && count<=99)
        {
            countStr="q-0"+countStr;
        }
        else
        {
            countStr="q-"+countStr;
        }
        resStr="<DOC "+countStr+">\n"+str+"\n</DOC>\n";
        output<<resStr;
    }
    
    input.close();
    output.close();
}

void
lss::Processing::wriDayStaEndDocID()
{
    std::string str;
    std::string subStr;
    std::string str2;
    std::string allFilNamStr;
    std::string monStr;
    std::string dayStr;
    std::string minTimFilStr;
    std::fstream input;
    std::fstream docInput;
    std::fstream output;
    
    unsigned long first;
    unsigned long second;
    unsigned long docNo;
    unsigned long minDocNo;
    
    std::vector<std::string> timVec;
    std::vector<std::string>::iterator it;
    
    allFilNamStr=lemur::api::ParamGetString("allFilNam");
    if(allFilNamStr.length()<=0)
    {
        std::cout<<"The parameter allFilNam has not been set."<<std::endl;
        return;
    }
    minTimFilStr=lemur::api::ParamGetString("minTimFil");
    if(minTimFilStr.length()<=0)
    {
        std::cout<<"The parameter minTimFil has not been set."<<std::endl;
        return;
    }
    input.open(allFilNamStr.c_str(), std::ios::in);
    output.open(minTimFilStr.c_str(), std::ios::out);
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        docInput.clear();
        first=str.find("tweets", 0);
        subStr=str.substr(first+7, 10);
        it=std::find(timVec.begin(), timVec.end(), subStr);
        if(it==timVec.end())
        {
            docInput.clear();
            docInput.open(str.c_str(), std::ios::in);
            if(!docInput)
            {
                std::cout<<"Can not open the file:"<<str<<std::endl;
                return;
            }
            minDocNo=1000000000000000000;
            while(docInput.eof()!=true)
            {
                std::getline(docInput, str2);
                if(str2.length()<=0)
                {
                    continue;
                }
                second=str2.find("<DOCNO>");
                if(second==0)
                {
                    std::stringstream ss;
                    ss<<str2.substr(8, 18);
                    ss>>docNo;
                    if(docNo==0)
                    {
                        std::cout<<"***********************"<<str2.substr(8, 18)<<std::endl;
                    }
                    if(docNo<minDocNo)
                    {
                        minDocNo=docNo;
                    }
                }
                
            }//while(docInput.eof()!=true)
            docInput.close();
            timVec.push_back(subStr);
            output<<subStr<<" "<<minDocNo<<std::endl;
        }//if(it==timVec.end())
    }//while(input.eof()!=true)
    input.close();
    output.close();
}

void
lss::Processing::wriDayStaEndDocID2()
{
    std::string str;
    std::string subStr;
    std::string str2;
    std::string allFilNamStr;
    std::string monStr;
    std::string dayStr;
    std::string minTimFilStr;
    std::fstream input;
    std::fstream docInput;
    std::fstream output;
    
    unsigned long first;
    unsigned long second;
    unsigned long docNo;
    unsigned long minDocNo;
    
    std::vector<std::string> timVec;
    std::vector<std::string>::iterator it;
    
    allFilNamStr=lemur::api::ParamGetString("allFilNam");
    if(allFilNamStr.length()<=0)
    {
        std::cout<<"The parameter allFilNam has not been set."<<std::endl;
        return;
    }
    minTimFilStr=lemur::api::ParamGetString("minTimFil");
    if(minTimFilStr.length()<=0)
    {
        std::cout<<"The parameter minTimFil has not been set."<<std::endl;
        return;
    }
    input.open(allFilNamStr.c_str(), std::ios::in);
    output.open(minTimFilStr.c_str(), std::ios::out);
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        docInput.clear();
        first=str.find("tweets", 0);
        subStr=str.substr(first+7, 10);
        it=std::find(timVec.begin(), timVec.end(), subStr);
        if(it==timVec.end())
        {
            docInput.clear();
            docInput.open(str.c_str(), std::ios::in);
            if(!docInput)
            {
                std::cout<<"Can not open the file:"<<str<<std::endl;
                return;
            }
            minDocNo=1000000000000000000;
            while(docInput.eof()!=true)
            {
                std::getline(docInput, str2);
                if(str2.length()<=0)
                {
                    continue;
                }
                second=str2.find("<DOCNO>");
                if(second==0)
                {
                    std::stringstream ss;
                    ss<<str2.substr(8, 18);
                    ss>>docNo;
                    
                    if(docNo<minDocNo)
                    {
                        minDocNo=docNo;
                    }
                    break;
                }
                
            }//while(docInput.eof()!=true)
            docInput.close();
            timVec.push_back(subStr);
            output<<subStr<<" "<<minDocNo<<std::endl;
        }//if(it==timVec.end())
    }//while(input.eof()!=true)
    input.close();
    output.close();
}

void
lss::Processing::itePriFil(std::string str)
{
    DIR *dir;
    struct dirent *ent;
    std::string folStr;
    std::vector<std::string> strVec;
    if((dir=opendir(str.c_str()))!=NULL)
    {
        strVec.clear();
        while((ent=readdir(dir))!=NULL)
        {
            if(ent->d_name[0]!='.')
            {
                folStr=str+"/"+ent->d_name;
                strVec.push_back(folStr);
                //this->itePriFil(folStr);
            }
        }
        std::sort(strVec.begin(), strVec.end());
        for(unsigned long i=0; i<strVec.size(); i++)
        {
            this->itePriFil(strVec[i]);
        }
        closedir(dir);
    }
    else
    {
        //std::cout<<str<<std::endl;
        this->filVec.push_back(str);
    }
}

std::string
lss::Processing::getPat(const std::string& str)
{
    std::size_t found = str.find_last_of("/\\");
    return str.substr(0, found);
    std::cout << " path: " << str.substr(0,found) << '\n';
    std::cout << " file: " << str.substr(found+1) << '\n';
}

std::string
lss::Processing::getFilNam(const std::string& str)
{
    std::size_t found = str.find_last_of("/\\");
    return str.substr(found+1);
}

void
lss::Processing::forInpDat()
{
    std::string str;
    std::string filNamStr;
    std::string filNamStr2;
    std::string queDocStr;
    std::string forQueDocStr;
    std::string docStr;
    std::string cmdStr;
    std::string cmdOutStr;
    std::vector<std::string> filVec;
    std::fstream input;
    std::fstream output;
    DIR *dir;
    struct dirent *ent;
    unsigned long first;
    unsigned long second;
    
    queDocStr=lemur::api::ParamGetString("queDocDir");
    forQueDocStr=lemur::api::ParamGetString("forQueDocDir");
    if((dir=opendir(queDocStr.c_str())) != NULL)
    {
        while((ent=readdir(dir))!=NULL)
        {
            if(ent->d_name[0]!='.')
            {
                filVec.push_back(ent->d_name);
            }
        }
    } //if((dir=opendir(queDocStr.c_str()))
    /*cmdStr=queDocStr+"/q-001";
    cmdStr="wc -l "+cmdStr+" | awk '{print $1}' ";
    cmdOutStr=this->getStdOutFroCom(cmdStr);
    std::cout<<std::endl;
    std::cout<<"cmdOutStr="<<cmdOutStr<<std::endl;*/
    
    for(unsigned long i=0; i<filVec.size(); i++)
    {
        filNamStr=queDocStr+"/"+filVec[i];
        input.clear();
        input.open(filNamStr.c_str(), std::ios::in);
        if(!input)
        {
            continue;
        }
        cmdStr="wc -l "+filNamStr+" | awk '{print $1}' ";
        cmdOutStr=this->getStdOutFroCom(cmdStr);
        filNamStr2=forQueDocStr+"/"+filVec[i];
        output.clear();
        output.open(filNamStr2.c_str(), std::ios::out);
        output<<cmdOutStr;
        std::cout<<"query="<<filVec[i]<<std::endl;
        while(input.eof()!=true)
        {
            std::getline(input, str);
            if(str.length()<=0)
            {
                continue;
            }
            first=str.find(" ", 0);
            second=str.rfind(" ", str.length()-1);
            docStr=str.substr(first+1, second-first-1);
            output<<docStr<<" "<<this->priDocCon(docStr)<<std::endl;
        }
        output<<std::endl;
        input.close();
        output.close();
    } //for(unsigned long i=0;
    
    
    
    
    /*if((dir = opendir(rooPatStr.c_str())) != NULL)
     {
     while((ent = readdir(dir)) != NULL)
     {
     if(ent->d_name[0]!='.')
     {
     mooVec.push_back(ent->d_name);
     }
     }
     }//if((dir = opendir
     closedir(dir); */
}

std::string
lss::Processing::priDocCon(std::string docStr)
{
    int docID=index_.document(docStr);
    int termID;
    double prob;
    std::string resStr;
    
    lemur::langmod::DocUnigramCounter collCounter(docID, index_);
    lemur::langmod::MLUnigramLM MLCollModel(collCounter, index_.termLexiconID());
    //lemur::langmod::UnigramLM *collModel=&MLCollModel;
    
    //collModel->startIteration();
    MLCollModel.startIteration();
    for(; MLCollModel.hasMore();)
    {
        MLCollModel.nextWordProb(termID, prob);
        resStr=resStr+index_.term(termID)+" ";
        //std::cout<<"term="<<index_.term(termID)<<" number of this term="<<index_.termCount(termID)<<" ";
    }
    return resStr.substr(0, resStr.length()-1);
}

std::string
lss::Processing::getStdOutFroCom(std::string cmd)
{
    std::string data;
    FILE* stream;
    const int max_buffer =512;
    char buffer[max_buffer];
    cmd.append(" 2>&1");
    
    stream=popen(cmd.c_str(), "r");
    if(stream)
    {
        while(!feof(stream))
        {
            if (fgets(buffer, max_buffer, stream) != NULL)
            {
                data.append(buffer);
            }
        }
        pclose(stream);
    }
    return data;
}

lss::DOCSCO_T
lss::Processing::getDocSco(std::string queDocFil, std::string thetaFil, const int topK)
{
    double sco;
    std::string str;
    std::string str1, str2;
    std::string docStr;
    unsigned long first, second;
    std::fstream input1, input2;
    std::stringstream ss;
    lss::DOCSCO_T dS;
    lss::DOCSCO_T result;
    lss::DOCSCO_T::iterator it;
    lss::DOCSCO_T allDocSco;
    std::vector<double> scoVec;
    double minCutSco;
    
    
    input1.open(queDocFil.c_str(), std::ios::in);
    if(!input1)
    {
        std::cout<<"Can not open the file: "<<queDocFil<<std::endl;
        return dS;
    }
    input2.open(thetaFil.c_str(), std::ios::in);
    if(!input2)
    {
        std::cout<<"Can not open the file: "<<thetaFil<<std::endl;
        return dS;
    }
    while(input1.eof()!=true)
    {
        std::getline(input1, str1);
        if(str1.length()<=0)
        {
            continue;
        }
        first=str1.find(' ');
        second=str1.rfind(' ');
        docStr=str1.substr(first+1, second-first-1);
        ss.clear();
        ss<<str1.substr(second+1, str1.length()-second-1);
        ss>>sco;
        allDocSco[docStr]=sco; //insert the doc-sco for all the docuemnts
    }
    while(input2.eof()!=true)
    {
        std::getline(input2, str2);
        if(str2.length()<=0)
        {
            continue;
        }
        first=str2.find(' ');
        docStr=str2.substr(0, first);
        it=allDocSco.find(docStr);
        if(it==allDocSco.end())
        {
            std::cout<<"Can not find the document ID"<<docStr<<" in: "<<queDocFil<<std::endl;
        }
        else
        {
            dS[docStr]=it->second;
            scoVec.push_back(it->second);
        }
    }
    std::sort(scoVec.begin(), scoVec.end());
    if(scoVec.size()<topK)
    {
        minCutSco=0.0;
    }
    else
    {
        minCutSco=scoVec[scoVec.size()-topK];
    }
    for(it=dS.begin(); it!=dS.end(); it++)
    {
        if(it->second>=minCutSco)
        {
            result[it->first]=it->second;
        }
    }
    
    input1.close();
    input2.close();
    return result;
}


lss::DOCSCO_T
lss::Processing::getDocSco_lan(std::string queDocFil, std::string thetaFil, const int topK)
{
    double sco;
    std::string str;
    std::string str1, str2;
    std::string scoStr;
    std::string docStr;
    unsigned long first, second;
    std::fstream input1, input2;
    std::stringstream ss;
    lss::DOCSCO_T dS;
    lss::DOCSCO_T result;
    lss::DOCSCO_T::iterator it;
    lss::DOCSCO_T allDocSco;
    std::vector<double> scoVec;
    double minCutSco;
    
    
    input1.open(queDocFil.c_str(), std::ios::in);
    if(!input1)
    {
        std::cout<<"Can not open the file: "<<queDocFil<<std::endl;
        return dS;
    }
    input2.open(thetaFil.c_str(), std::ios::in);
    if(!input2)
    {
        std::cout<<"Can not open the file: "<<thetaFil<<std::endl;
        return dS;
    }
    while(input1.eof()!=true)
    {
        std::getline(input1, str1);
        if(str1.length()<=0)
        {
            continue;
        }
        first=str1.find(' ');
        second=str1.rfind(' ');
        docStr=str1.substr(first+1, second-first-1);
        //ss.clear();
        //ss<<str1.substr(second+1, str1.length()-second-1);
        //ss>>sco;
        scoStr=str1.substr(second+1, str1.length()-second-1);
        sco=std::atof(scoStr.c_str());
        allDocSco[docStr]=sco; //insert the doc-sco for all the docuemnts
    }
    while(input2.eof()!=true)
    {
        std::getline(input2, str2);
        if(str2.length()<=0)
        {
            continue;
        }
        first=str2.find(' ');
        docStr=str2.substr(0, first);
        it=allDocSco.find(docStr);
        if(it==allDocSco.end())
        {
            std::cout<<"Can not find the document ID "<<docStr<<" in: "<<queDocFil<<std::endl;
        }
        else
        {
            dS[docStr]=it->second;
            scoVec.push_back(it->second);
        }
    }
    std::sort(scoVec.begin(), scoVec.end());
    if(scoVec.size()<topK)
    {
        minCutSco=0.0;
    }
    else
    {
        minCutSco=scoVec[scoVec.size()-topK];
    }
    for(it=dS.begin(); it!=dS.end(); it++)
    {
        if(it->second>=minCutSco)
        {
            result[it->first]=it->second;
        }
    }
    
    input1.close();
    input2.close();
    return result;
}


void
lss::Processing::combResLisFroEachQue(std::string resDir, std::string resDir2)
{
    bool isExi2;
    unsigned int index;
    DIR *dir;
    struct dirent *ent;
    std::vector<std::string> queVec;
    std::vector<std::string> datVec;
    std::string str;
    std::string str2;
    //std::string strTemp;
    std::string datStr;
    std::string cmdStr;
    std::fstream input;
    std::string ranFilNam=lemur::api::ParamGetString("mp2RankFil");
    
    if((dir=opendir(resDir.c_str())) !=NULL)
    {
        while((ent = readdir(dir)) != NULL)
        {
            if(ent->d_name[0]!='.')
            {
                queVec.push_back(ent->d_name);
            }
        }
        closedir(dir);
    }
    else
    {
        std::cout<<"Can not find the folder: "<<std::endl;
    }
    
    
    for(unsigned long i=0; i<queVec.size(); i++)
    {
        str=resDir+"/"+queVec[i];
        if((dir=opendir(str.c_str()))!=NULL)
        {
            
            while((ent = readdir(dir)) !=NULL)
            {
                isExi2=false;
                if(ent->d_name[0]!='.')
                {
                    //datStr=ent->d_name;
                    std::string strTemp=std::string(ent->d_name);
                    datStr=strTemp;
                    for(index=0; index<datVec.size(); index++)
                    {
                        if(strTemp==datVec[index])
                        {
                            isExi2=true;
                            //std::cout<<"*="<<datStr<<std::endl;
                        }
                    }
                    if(isExi2==false)
                    {
                        datVec.push_back(datStr);
                    }
                }
            }
            
            closedir(dir);
        }
    }
    //std::cout<<"*********="<<datVec.size()<<std::endl;
    for(unsigned long i=0; i<datVec.size(); i++)
    {
        str2=resDir2+"/"+datVec[i];
        input.open(str2.c_str(), std::ios::in);
        if(!input)
        {
            cmdStr="mkdir "+str2;
            std::system(cmdStr.c_str());
        }
        else
        {
            input.close();
        }
        cmdStr="cat "+resDir+"/*/"+datVec[i]+"/* > "+ str2+"/"+datVec[i]+"."+ranFilNam;
        std::cout<<"cmdStr="<<cmdStr<<std::endl;
        std::system(cmdStr.c_str());
    }
    
    
    
    
    
    /*std::string pathStr=lemur::api::ParamGetString("path");
    filVec.clear();
    if((dir = opendir (pathStr.c_str())) != NULL)
    {
        while((ent = readdir (dir)) != NULL)
        {
            if(ent->d_name[0]!='.')
            {
                filVec.push_back(ent->d_name);
            }
        }
        closedir(dir);
    }
    else
    {
        std::cout<<"Can not find the folder: "<<pathStr<<std::endl;
        return;
    }*/
}



void
lss::Processing::forResFilForQue(std::string retrResDir1, std::string retrResDir3)
{
    std::string str;
    std::string folStr1;
    std::string filNam1, filNam2;
    DIR *dir1, *dir2;
    struct dirent *ent1, *ent2;
    
    if((dir1 = opendir(retrResDir1.c_str())) != NULL)
    {
        while((ent1 = readdir(dir1)) != NULL)
        {
            if(ent1->d_name[0]!='.' && ent1->d_type==DT_DIR)
            {
                folStr1=retrResDir1+"/"+ent1->d_name;
                if((dir2 = opendir(folStr1.c_str())) != NULL)
                {
                    while((ent2 = readdir(dir2)) != NULL)
                    {
                        if(ent2->d_name[0]!='.')
                        {
                            filNam1=retrResDir1+"/"+ ent1->d_name+"/"+ent2->d_name;
                            filNam2=retrResDir3+"/"+ ent1->d_name+"/"+ent2->d_name;
                            std::cout<<filNam1<<std::endl;
                            std::cout<<filNam2<<std::endl;
                            this->forResFilForQue2(filNam1, filNam2);
                        } // if(ent2->d_name[0]!='.')
                    }
                }
            }
        } //while((ent1 = readdir(dir1)) != NULL)
    } // if((dir1 = opendir(retrResDir1.c_str())) != NULL)
    else
    {
        std::cout<<"Can not find the folder: "<<retrResDir1<<std::endl;
        return;
    }
}

void
lss::Processing::forResFilForQue2(std::string filStr1, std::string filStr2)
{
    std::string str;
    std::string str2;
    std::string extQStr; //extract query string for diversification evaluation
    std::fstream input, output;
    unsigned long first, second;
    int queInt;
    
    input.open(filStr1.c_str(), std::ios::in);
    if(!input)
    {
        std::cout<<"Can not open the file: "<<filStr1<<std::endl;
        return;
    }
    output.open(filStr2.c_str(), std::ios::out);
    if(!output)
    {
        std::cout<<"Can not write to the file: "<<filStr2<<std::endl;
        return;
    }
    
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        first=str.find('-', 0);
        second=str.find(' ', 0);
        extQStr=str.substr(first+1, second-first-1);
        queInt=(int)std::atof(extQStr.c_str());
        str2=str.substr(second+1, str.length()-second-1);
        output<<queInt<<" "<<str2<<std::endl;
    }
    
    input.close();
    output.close();
}



























