//
//  ResultWriter.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 9/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "ResultWriter.h"
#include "DataTypesTwo.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <utility>


// Lemur
#include "Param.hpp"

void
lss::ResultWriter::writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, const std::string subResPat)
{
    lss::QUEDOCSCO_T::iterator it;
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSTRSCO_T dS_str;//a document-score struct
    lss::DOCSTRSCO_T strTemp; //a temporal struct
    std::string savFil;
    std::fstream outputFil;
    std::string comStr="";//command string for writing evaluation file
    std::string trecEvalParStr=TRECPARMETER;
    std::string savEvaFil;
    int rank=0;
    
    savPath_=lemur::api::ParamGetString("resultPath", "result");//default path is 'result'
    if(subResPat == "")
    {
        savFil=savPath_+"/"+filNam;
    }
    else
    {
        savFil=savPath_+"/"+subResPat+"/"+filNam;
    }
    qrelFil_=lemur::api::ParamGetString("qrelGT");
    if(qrelFil_.length()<=0)
    {
        std::cout<<"Can not read the qrel file path, please check the parameter file."<<std::endl;
        return;
    }
    trecEvalFil_=lemur::api::ParamGetString("trecEval");
    if(trecEvalFil_.length()<=0)
    {
        std::cout<<"Can not read the trec_eval file path, please check the parameter file."<<std::endl;
        return;
    }
    outputFil.open(savFil.c_str(), std::ios::out);
    if(!outputFil)
    {
        std::cout<<"Can not write to the file: "<<savFil<<". please check the parameter file."<<std::endl;
        return;
    }
    for(it=qDS.begin(); it!=qDS.end(); it++)
    {
        std::vector<lss::DOCSTRSCO_T> dS_vec;
        for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
        {
            dS_str.docStr=it_dS->first;
            dS_str.sco=it_dS->second;
            dS_vec.push_back(dS_str);
        }
        for(long int i=dS_vec.size()-1; i>=0; i--)//sorting
        {
            for(long int j=0; j<i; j++)
            {
                if(dS_vec[j].sco<dS_vec[j+1].sco)
                {
                    strTemp.docStr=dS_vec[j].docStr;
                    strTemp.sco=dS_vec[j].sco;
                    dS_vec[j].docStr=dS_vec[j+1].docStr;
                    dS_vec[j].sco=dS_vec[j+1].sco;
                    dS_vec[j+1].docStr=strTemp.docStr;
                    dS_vec[j+1].sco=strTemp.sco;
                }
            }
        }//for(long int i=dS_vec.size()-1; i>=0; i--)
        for(int i=0; i<dS_vec.size(); i++)
        {
            rank=i+1;
            outputFil<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
            //std::cout<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
        }
    }//for(it=qDS.begin(); it!=qDS.end(); it++)
    outputFil.close();
    savEvaFil=savFil+".eva";
    comStr=trecEvalFil_+" "+trecEvalParStr+" "+qrelFil_+" "+savFil+" >"+savEvaFil;
    std::system(comStr.c_str());
}

/*
 void
 lss::ResultWriter::writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, lss::QUEDOCSCO_T& qTopDS, const double percent)
 {
 lss::QUEDOCSCO_T::iterator it;
 lss::QUEDOCSCO_T::iterator itQTopDS;
 lss::DOCSCO_T::iterator it_dS;
 lss::DOCSTRSCO_T dS_str;//a document-score struct
 lss::DOCSTRSCO_T strTemp; //a temporal struct
 std::string savFil;
 std::fstream outputFil;
 std::string comStr="";//command string for writing evaluation file
 std::string trecEvalParStr=TRECPARMETER;
 std::string savEvaFil;
 int rank=0;
 
 savPath_=lemur::api::ParamGetString("resultPath", "result");//default path is 'result'
 savFil=savPath_+"/"+filNam;
 qrelFil_=lemur::api::ParamGetString("qrelGT");
 if(qrelFil_.length()<=0)
 {
 std::cout<<"Can not read the qrel file path, please check the parameter file."<<std::endl;
 return;
 }
 trecEvalFil_=lemur::api::ParamGetString("trecEval");
 if(trecEvalFil_.length()<=0)
 {
 std::cout<<"Can not read the trec_eval file path, please check the parameter file."<<std::endl;
 return;
 }
 outputFil.open(savFil.c_str(), std::ios::out);
 if(!outputFil)
 {
 std::cout<<"Can not write to the file: "<<savFil<<". please check the parameter file."<<std::endl;
 return;
 }
 for(it=qDS.begin(); it!=qDS.end(); it++)
 {
 std::vector<lss::DOCSTRSCO_T> dS_vec;
 lss::DOCSCO_T dS;
 for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
 {
 dS_str.docStr=it_dS->first;
 dS_str.sco=it_dS->second;
 dS_vec.push_back(dS_str);
 }
 for(long int i=dS_vec.size()-1; i>=0; i--)//sorting
 {
 for(long int j=0; j<i; j++)
 {
 if(dS_vec[j].sco<dS_vec[j+1].sco)
 {
 strTemp.docStr=dS_vec[j].docStr;
 strTemp.sco=dS_vec[j].sco;
 dS_vec[j].docStr=dS_vec[j+1].docStr;
 dS_vec[j].sco=dS_vec[j+1].sco;
 dS_vec[j+1].docStr=strTemp.docStr;
 dS_vec[j+1].sco=strTemp.sco;
 }
 }
 }//for(long int i=dS_vec.size()-1; i>=0; i--)
 for(int i=0; i<dS_vec.size(); i++)
 {
 rank=i+1;
 outputFil<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
 //std::cout<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
 }
 for(int i=0; i<dS_vec.size() && i<=percent*dS_vec.size(); i++)
 {
 dS.insert(DOCSCO_PAIR_T(dS_vec[i].docStr, dS_vec[i].sco));
 }
 qTopDS.insert(QUEDOCSCO_PAIR_T(it->first, dS));
 }//for(it=qDS.begin(); it!=qDS.end(); it++)
 outputFil.close();
 savEvaFil=savFil+".eva";
 comStr=trecEvalFil_+" "+trecEvalParStr+" "+qrelFil_+" "+savFil+" >"+savEvaFil;
 std::system(comStr.c_str());
 }
 */

void
lss::ResultWriter::writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, lss::QUEDOCSCO_T& qTopDS, const double topDocNum, bool isForDiv)
{
    lss::QUEDOCSCO_T::iterator it;
    lss::QUEDOCSCO_T::iterator itQTopDS;
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSTRSCO_T dS_str;//a document-score struct
    lss::DOCSTRSCO_T strTemp; //a temporal struct
    std::string savFil;
    std::fstream outputFil;
    std::string comStr="";//command string for writing evaluation file
    std::string trecEvalParStr=TRECPARMETER;
    std::string savEvaFil;
    int rank=0;
    
    savPath_=lemur::api::ParamGetString("resultPath", "result");//default path is 'result'
    savFil=savPath_+"/"+filNam;
    qrelFil_=lemur::api::ParamGetString("qrelGT");
    if(qrelFil_.length()<=0)
    {
        std::cout<<"Can not read the qrel file path, please check the parameter file."<<std::endl;
        return;
    }
    qrelGFil_=lemur::api::ParamGetString("qrelGdeval");
    if(qrelGFil_.length()<=0)
    {
        std::cout<<"Can not read the gdeval.pl ground truth file, please check the parameter file."<<std::endl;
        return;
    }
    qrelNFil_=lemur::api::ParamGetString("qrelNdeval");
    if(qrelNFil_.length()<=0)
    {
        std::cout<<"Can not read the ndeval ground truth file, please check the parameter file."<<std::endl;
        return;
    }
    trecEvalFil_=lemur::api::ParamGetString("trecEval");
    if(trecEvalFil_.length()<=0)
    {
        std::cout<<"Can not read the trec_eval file path, please check the parameter file."<<std::endl;
        return;
    }
    gdevalEvalFil_=lemur::api::ParamGetString("gdEval");
    if(gdevalEvalFil_.length()<=0)
    {
        std::cout<<"Can not read the gdeval.pl file, please check the parameter file."<<std::endl;
        return;
    }
    ndevalEvalFil_=lemur::api::ParamGetString("ndEval");
    if(ndevalEvalFil_.length()<=0)
    {
        std::cout<<"Can not read the ndeval file, please check the parameter file."<<std::endl;
        return;
    }
    outputFil.open(savFil.c_str(), std::ios::out);
    if(!outputFil)
    {
        std::cout<<"Can not write to the file: "<<savFil<<". please check the parameter file."<<std::endl;
        return;
    }
    for(it=qDS.begin(); it!=qDS.end(); it++)
    {
        std::vector<lss::DOCSTRSCO_T> dS_vec;
        lss::DOCSCO_T dS;
        for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
        {
            dS_str.docStr=it_dS->first;
            dS_str.sco=it_dS->second;
            dS_vec.push_back(dS_str);
        }
        for(long int i=dS_vec.size()-1; i>=0; i--)//sorting
        {
            for(long int j=0; j<i; j++)
            {
                if(dS_vec[j].sco<dS_vec[j+1].sco)
                {
                    strTemp.docStr=dS_vec[j].docStr;
                    strTemp.sco=dS_vec[j].sco;
                    dS_vec[j].docStr=dS_vec[j+1].docStr;
                    dS_vec[j].sco=dS_vec[j+1].sco;
                    dS_vec[j+1].docStr=strTemp.docStr;
                    dS_vec[j+1].sco=strTemp.sco;
                }
            }
        }//for(long int i=dS_vec.size()-1; i>=0; i--)
        for(int i=0; i<dS_vec.size(); i++)
        {
            rank=i+1;
            outputFil<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
            //std::cout<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
        }
        for(int i=0; i<dS_vec.size() && i<topDocNum; i++)
        {
            dS.insert(DOCSCO_PAIR_T(dS_vec[i].docStr, dS_vec[i].sco));
        }
        qTopDS.insert(QUEDOCSCO_PAIR_T(it->first, dS));
    }//for(it=qDS.begin(); it!=qDS.end(); it++)
    outputFil.close();
    savEvaFil=savFil+".eva";
    comStr=trecEvalFil_+" "+trecEvalParStr+" "+qrelFil_+" "+savFil+" >"+savEvaFil;
    std::system(comStr.c_str());
    if(isForDiv==true)
    {
        savEvaFil=savFil+".gEva";
        comStr=gdevalEvalFil_+" "+qrelGFil_+" "+savFil+" >"+savEvaFil;
        std::system(comStr.c_str());
        savEvaFil=savFil+".nEva";
        comStr=ndevalEvalFil_+" "+qrelNFil_+" "+savFil+" >"+savEvaFil;
        std::system(comStr.c_str());
    }
}

bool
lss::ResultWriter::dirExists(const char* dirPath)
{
    if(dirPath == NULL)
    {
        return false;
    }
    DIR *pDir;
    bool bExists = false;
    pDir=opendir(dirPath);
    if(pDir != NULL)
    {
        bExists = true;
        (void) closedir(pDir);
    }
    return bExists;
}

void
lss::ResultWriter::wriQueLDAFil(lss::QUEDOCSCO_T& qTopDS)
{
    std::string ldaDir;
    std::string subLdaDir;
    std::string savLdaDir;
    std::string savFilstr;
    std::string filNam;
    std::string str;
    std::string aStr="en0011";
    std::string bStr="enwp00";
    std::fstream output;
    std::fstream input;
    lss::QUEDOCSCO_T::iterator qDS_it;
    lss::DOCSCO_T::iterator dS_it;
    
    ldaDir=lemur::api::ParamGetString("ldaForDocDir");
    if(ldaDir.length()<=0)
    {
        std::cout<<"Parameter 'ldaForDocDir' is not set."<<std::endl;
        return;
    }
    savLdaDir=lemur::api::ParamGetString("savQueLdaDocDir");
    if(savLdaDir.length()<=0)
    {
        std::cout<<"Parameter 'savQueLdaDocDir' is not set."<<std::endl;
        return;
    }
    for(qDS_it=qTopDS.begin(); qDS_it!=qTopDS.end(); qDS_it++)
    {
        savFilstr=savLdaDir+"/"+"q"+qDS_it->first;
        output.open(savFilstr.c_str(), std::ios::out);
        output<<qDS_it->second.size()<<std::endl;
        for(dS_it=qDS_it->second.begin(); dS_it!=qDS_it->second.end(); dS_it++)
        {
            subLdaDir=dS_it->first.substr(10, 6); //sub dir to get the lda-formated file
            if(aStr.compare(subLdaDir)<0 && bStr.compare(subLdaDir)>0)
            {
                filNam=ldaDir+"/enOther/"+dS_it->first;
            }
            else
            {
                filNam=ldaDir+"/"+subLdaDir+"/"+dS_it->first; //en0000~en0011, enwp00~enwp03
            }
            input.open(filNam.c_str(), std::ios::in);
            if(!input)
            {
                std::cout<<"Can't open the file: "<<dS_it->first<<" in the folder: "<<subLdaDir<<std::endl;
                //return; //**
            }
            if(input)//**
            {
                std::getline(input, str);//**
            }
            
            output<< str<<std::endl;
            input.close();
        }
        output.close();
    }
}

void
lss::ResultWriter::writeRank(std::string que, lss::DOCSCO_T dS, const std::string filNam, const std::string ranNam)
{
    std::string str;
    std::string str2, str3;
    std::stringstream ss;
    const int numOfDoc=(int)dS.size();
    lss::DOCSCO_T::iterator it;
    std::fstream input;
    int index;
    std::vector<lss::DOCSTRSCO_T> dS_vec;
    lss::DOCSTRSCO_T dS_str, strTemp; //document-score struct
    //FILE * fout = fopen(filNam.c_str(), "a+");
    
    //input.open(filNam.c_str(), std::ios::app);
    input.open(filNam.c_str(), std::ios::out);
    if(!input)
    {
        std::cout<<"Can not open the file: "<<filNam<<std::endl;
        return;
    }
    index=1;
    for(it=dS.begin(); it!=dS.end(); it++)
    {
        dS_str.docStr=it->first;
        dS_str.sco=it->second;
        dS_vec.push_back(dS_str);
    }
    for(long int i=dS_vec.size()-1; i>=0; i--)//sorting
    {
        for(long int j=0; j<i; j++)
        {
            if(dS_vec[j].sco<dS_vec[j+1].sco)
            {
                strTemp.docStr=dS_vec[j].docStr;
                strTemp.sco=dS_vec[j].sco;
                dS_vec[j].docStr=dS_vec[j+1].docStr;
                dS_vec[j].sco=dS_vec[j+1].sco;
                dS_vec[j+1].docStr=strTemp.docStr;
                dS_vec[j+1].sco=strTemp.sco;
            }
        }
    }//for(long int i=dS_vec.size()-1; i>=0; i--)
    
    for(long int i=0; i<dS_vec.size(); i++)
    {
        input<<que<<" Q0 "<<dS_vec[i].docStr<<" "<<i<<" "<<dS_vec[i].sco<<" "<<ranNam<<std::endl;
        //std::cout<<que<<" Q0 "<<dS_vec[i].docStr<<" "<<i<<" "<<dS_vec[i].sco<<" "<<ranNam<<std::endl;
        //fprintf(fout,"%s Q0 %s %d %f %s\n", que.c_str(), dS_vec[i].docStr.c_str(), (int)i, dS_vec[i].sco, ranNam.c_str());
    }
    input.close();
    //fclose(fout);
    
}

/*void
lss::ResultWriter::writeRank(lss::QUEDOCSCO_T qDS, const std::string dir)
{
    lss::QUEDOCSCO_T::iterator it;
    lss::DOCSCO_T::iterator dS_it;
    
    for(it=qDS.begin(); it!=qDS.end(); it++)
    {
        for(dS_it=it->second.begin(); )
        {
            
        }
    } //for(it=qDS.begin();
}*/



















