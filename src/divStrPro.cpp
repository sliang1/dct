//
//  divStrPro.cpp
//  DCT and its application to Diversification
//
//  Created by Shangsong Liang on 7/14/15.
//  Copyright (c) 2015 ___ShangsongLiang___. All rights reserved.
//

#include "DataTypesTwo.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>

#include "Param.hpp"
#include "Processing.h"
#include "DiversifyStream.h"
#include "QueryRep.h"
#include "Framework.h"

void usage() {
    std::cerr<<std::endl << "USAGE: ./clustering configure-file" << std::endl
    << std::endl
    << "  Command description:" << std::endl
    << "  --------------------" << std::endl
    << "  The defaul parameter will select and merge the ranked listed randomly." << std::endl
    << "  -m fixed -t value means select and merge the top best ranked lists. Here VALUE is the number of ranked lists which are used." << std::endl
    <<"   -m fixed -b value means select and merge the bottom worst ranked lists. Here VALUE is the number of ranked lists which are used."
    << std::endl
    << std::endl;
}

int main (int argc, const char * argv[])
{
    try {
        if(lemur::api::ParamPushFile(argv[1]))
        {
            std::cout<<"Import parameters for running successfully."<<std::endl;
        }
        else
        {
            std::cout<<"Can not import parameters."<<std::endl;
            usage();
            return 0;
        }
    } catch (const std::exception& e) {
        usage();
        std::cout<<e.what()<<std::endl;
    }
    
    //lss::Processing a=lss::Processing();
    //lss::DiversifyStream divStr=lss::DiversifyStream();
    lss::Framework framework;
    framework.run();
    
    std::cout<<"End"<<std::endl;
}






